<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Thrift_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getProduct($product_id)
    {
        $this->db->select('*');
        $this->db->from('pg_product as pr');
        $this->db->where('pr.product_id', $product_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function getUserDetail($employee_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $employee_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function getIncompleteThriftGroups($product_id, $colleagues)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_product_id', $product_id);

        $this->db->where('tg.thrift_group_member_count  < tg.thrift_group_member_limit');
        $this->db->where('tg.thrift_group_open', 1);

        /*extra starts*/
        if ($colleagues) {
            $thrifts_query =
                'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . implode(',', $colleagues) . ')';

            $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);
        }
        /*extra ends*/


        $query = $this->db->get();

        $result = $query->result();
        return $result;

    }

    public function countAllIncompleteThriftGroups()
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_member_count  < tg.thrift_group_member_limit');
        $this->db->where('tg.thrift_group_open', 1);

        $query = $this->db->get();

        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getAllIncompleteThriftGroups()
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_member_count  < tg.thrift_group_member_limit');
        $this->db->where('tg.thrift_group_open', 1);

        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    public function countAllIncompleteThriftGroupsCrossingThreshold($curr_dt)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_member_count  < tg.thrift_group_member_limit');
        $this->db->where('tg.thrift_group_open', 1);
        $this->db->where('tg.thrift_group_is_custom_product', 0);
        $this->db->where('tg.thrift_group_threshold_date  <', $curr_dt);

        $query = $this->db->get();

        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getAllIncompleteThriftGroupsCrossingThreshold($curr_dt)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_member_count  < tg.thrift_group_member_limit');
        $this->db->where('tg.thrift_group_open', 1);
        $this->db->where('tg.thrift_group_threshold_date  <', $curr_dt);

        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    public function getActiveBotAdminsIds($limit)
    {
        $this->db->select('u.id as user_id');
        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->where('u.is_user_bot', 1);
        $this->db->where('u.deletion_status !=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 1);

        $this->db->limit($limit);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $result = $query->result();
        return $result;
    }

    public function getSystemAdminWithActiveBots()
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');
        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->group_start();
        $this->db->where('u.is_user_bot', 1);
        $this->db->or_where('u.id', 1);
        $this->db->group_end();
        $this->db->where('u.deletion_status !=', 1);
        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 1);

        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }


    /*actually*/
    public function checkIfMemberAlreadyInThriftGroup($thrift_group_id, $member_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_members as tgm');

        $this->db->where('tgm.thrift_group_id', $thrift_group_id);
        $this->db->where('tgm.thrift_group_member_id', $member_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getJoinedThrifts($employee_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->join('pg_thrift_group_members as tgm', 'tg.thrift_group_id = tgm.thrift_group_id');
        $this->db->where('tgm.thrift_group_member_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getRunningJoinedThrifts($employee_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_current_cycle < tg.thrift_group_term_duration');
        $this->db->join('pg_thrift_group_members as tgm', 'tg.thrift_group_id = tgm.thrift_group_id');
        $this->db->where('tgm.thrift_group_member_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getThriftGroup($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function insertThriftGroup($ins_data)
    {
        $this->db->insert('pg_thrift_group', $ins_data);

        return $this->db->insert_id();


    }

    public function insertUserInThriftGroup($ins_data)
    {
        $this->db->insert('pg_thrift_group_members', $ins_data);
        return true;
    }

    public function updateThriftGroup($upd_data, $thrift_group_id)
    {
        $this->db->where('thrift_group_id', $thrift_group_id);
        $this->db->update('pg_thrift_group', $upd_data);
    }

    public function updateCustomProductInvitation($upd_data, $cpi_id)
    {
        $this->db->where('cpi_id', $cpi_id);
        $this->db->update('pg_custom_product_invitation', $upd_data);
    }

    public function updateThriftGroupMember($upd_data, $thrift_group_id, $member_id)
    {
        $this->db->where('thrift_group_id', $thrift_group_id);
        $this->db->where('thrift_group_member_id', $member_id);
        $this->db->update('pg_thrift_group_members', $upd_data);
    }

    public function updateThriftGroupEndDate($thrift_group_end_date, $thrift_group_id)
    {
        $this->db->set('thrift_group_end_date', $thrift_group_end_date, FALSE);
        $this->db->where('thrift_group_id', $thrift_group_id);
        $this->db->update('pg_thrift_group'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
    }

    public function insertThriftGroupPaymentSetup($ins_data)
    {
        $this->db->insert('pg_thrift_group_payments', $ins_data);

        return true;
    }

    public function insertThriftGroupPaymentRecieveSetup($ins_data)
    {
        $this->db->insert('pg_thrift_group_payment_recieves', $ins_data);
        return true;
    }

    public function getAllProducts()
    {
        $this->db->select('*');
        $this->db->from('pg_product as pr');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/


    public function getAllThriftIdsByEmployee($employee_id)
    {
        $this->db->select('tgm.thrift_group_id');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where('tgm.thrift_group_member_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAllMyEmployeesIds($employer_id)
    {
        $this->db->select('id');
        $this->db->from('users as u');
        $this->db->where('user.deletion_status!=', 1);

        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');
        $this->db->where('ud.user_employer_id', $employer_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAllThriftIdsByEmployees($employee_ids)
    {
        $this->db->select('tgm.thrift_group_id');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where_in('tgm.thrift_group_member_id', $employee_ids);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function countThrifts($common_filter_value = false, $specific_filters = false, $which_list, $user_id)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,
                        tg.thrift_group_product_id,
                        tg.thrift_group_product_price,
                        
                        tg.thrift_group_member_count,
                        tg.thrift_group_member_limit,
                        
                        tg.thrift_group_current_cycle,
                        tg.thrift_group_term_duration,
                        
                        tg.thrift_group_creation_date,
                        tg.thrift_group_start_date,
                        
                        tg.thrift_group_open,
                        tg.thrift_group_activation_status,
                        tg.thrift_group_deletion_status,
                        
        ', false);


        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);

        /*extra starts*/
        $my_thrifts_query = 'Select u.id from users as u where u.deletion_status!=1 and u.id=' . $user_id;

        $my_employees_thrifts_query =
            'Select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $user_id . ')';


        if ($which_list == 'my' || $which_list == 'my_employees') {

            $extra_user_query = '';

            if ($which_list == 'my') {
                $extra_query = $my_thrifts_query;
            }

            if ($which_list == 'my_employees') {
                $extra_query = $my_employees_thrifts_query;
            }

            $thrifts_query =
                'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $extra_query . ')';


            $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);
        }
        /*extra ends*/

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {

            $this->db->like('tg.thrift_group_number', $common_filter_value);

        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_product_price') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_product_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_product_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_activation_status') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.' . $column_name, 1);
                    } else {
                        $this->db->where('tg.' . $column_name . '!=', 1);
                    }

                }

                if ($column_name == 'thrift_group_open') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.' . $column_name, 1);
                    } else {
                        $this->db->where('tg.' . $column_name . '!=', 1);
                    }

                }

                if ($column_name == 'thrift_group_completion') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.thrift_group_current_cycle=tg.thrift_group_term_duration');
                    } else {
                        $this->db->where('tg.thrift_group_current_cycle!=tg.thrift_group_term_duration');
                    }

                }

                if ($column_name == 'thrift_group_member_status') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.thrift_group_member_count=tg.thrift_group_member_limit');
                    } else {
                        $this->db->where('tg.thrift_group_member_count!=tg.thrift_group_member_limit');
                    }

                }


            }

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


    public function getThrifts($common_filter_value = false, $specific_filters = false, $order, $limit, $which_list, $user_id)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,
                        tg.thrift_group_product_id,
                        tg.thrift_group_product_price,
                        
                        tg.thrift_group_member_count,
                        tg.thrift_group_member_limit,
                        
                        tg.thrift_group_current_cycle,
                        tg.thrift_group_term_duration,
                        
                        tg.thrift_group_creation_date,
                        tg.thrift_group_start_date,
                        
                        tg.thrift_group_open,
                        tg.thrift_group_activation_status,
                        tg.thrift_group_deletion_status,
                        
        ', false);


        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);

        /*extra starts*/
        $my_thrifts_query = 'Select u.id from users as u where u.deletion_status!=1 and u.id=' . $user_id;

        $my_employees_thrifts_query =
            'Select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $user_id . ')';


        if ($which_list == 'my' || $which_list == 'my_employees') {

            $extra_user_query = '';

            if ($which_list == 'my') {
                $extra_query = $my_thrifts_query;
            }

            if ($which_list == 'my_employees') {
                $extra_query = $my_employees_thrifts_query;
            }

            $thrifts_query =
                'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $extra_query . ')';


            $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);
        }
        /*extra ends*/

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {

            $this->db->like('tg.thrift_group_number', $common_filter_value);

        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_product_price') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_product_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_product_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_activation_status') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.' . $column_name, 1);
                    } else {
                        $this->db->where('tg.' . $column_name . '!=', 1);
                    }

                }

                if ($column_name == 'thrift_group_open') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.' . $column_name, 1);
                    } else {
                        $this->db->where('tg.' . $column_name . '!=', 1);
                    }

                }

                if ($column_name == 'thrift_group_completion') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.thrift_group_current_cycle=tg.thrift_group_term_duration');
                    } else {
                        $this->db->where('tg.thrift_group_current_cycle!=tg.thrift_group_term_duration');
                    }

                }

                if ($column_name == 'thrift_group_member_status') {
                    if ($filter_value == 'yes') {
                        $this->db->where('tg.thrift_group_member_count=tg.thrift_group_member_limit');
                    } else {
                        $this->db->where('tg.thrift_group_member_count!=tg.thrift_group_member_limit');
                    }

                }


            }

        }

        // because 'thrift_group_completion' is not a tbl column
        if ($order['column'] == 'thrift_group_completion') {
            $order['column'] = 'thrift_group_current_cycle';
        }

        // because 'thrift_group_member_status' is not a tbl column
        if ($order['column'] == 'thrift_group_member_status') {
            $order['column'] = 'thrift_group_member_count';
        }

        $this->db->order_by('tg.' . $order['column'], $order['by']);

        $this->db->limit($limit['length'], $limit['start']);


        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function updateThriftGroupPayment($upd_data, $thrift_group_payment_id)
    {
        $this->db->where('thrift_group_payment_id', $thrift_group_payment_id);
        $this->db->update('pg_thrift_group_payments', $upd_data);

    }

    public function updateThriftGroupPaymentRecieve($upd_data, $thrift_group_payment_recieve_id)
    {
        $this->db->where('thrift_group_payment_recieve_id', $thrift_group_payment_recieve_id);
        $this->db->update('pg_thrift_group_payment_recieves', $upd_data);

    }

    /*-----------------------------------------------------------------------------------------------------------------*/

    public function getThriftMembers($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where('tgm.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getThriftGroupMember($member_id, $thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where('tgm.thrift_group_member_id', $member_id);
        $this->db->where('tgm.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getThriftGroupPayments($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tgp');
        $this->db->where('tgp.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getThriftGroupPaymentRecieves($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tgr');
        $this->db->where('tgr.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUnpaidThriftGroupPaymentsTillNow($current_timestamp)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tgp');
        $this->db->where('tgp.thrift_group_is_payment_paid!=', 1);
        $this->db->where('tgp.thrift_group_payment_date <', $current_timestamp);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUnpaidThriftGroupPaymentRecievesTillNow($current_timestamp)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tgr');
        $this->db->where('tgr.thrift_group_is_payment_recieved!=', 1);
        $this->db->where('tgr.thrift_group_payment_date <', $current_timestamp);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getMember($member_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $member_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function get_this_month_payment($thrift_group_id)
    {
        $get_month_wise = date('Y-m'); // Get current year and subtract 1
        $start = $get_month_wise . '-01';
        $end = $get_month_wise . '-31';

        $this->db->select('count(thrift_group_payment_amount) as total_payment_count, SUM(thrift_group_payment_amount) as total_payment_amount');
        $this->db->from('pg_thrift_group_payments');

        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")>=', $start);
        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")<=', $end);
        $this->db->where('pg_thrift_group_payments.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function get_this_month_disbursement($thrift_group_id)
    {
        $get_month_wise = date('Y-m'); // Get current year and subtract 1
        $start = $get_month_wise . '-01';
        $end = $get_month_wise . '-31';

        $this->db->select('count(thrift_group_payment_recieve_amount) as total_payment_recieve_count, SUM(thrift_group_payment_recieve_amount) as total_payment_recieve_amount');
        $this->db->from('pg_thrift_group_payment_recieves');

        $this->db->where('from_unixtime(pg_thrift_group_payment_recieves.thrift_group_payment_date, "%Y-%m-%d")>=', $start);
        $this->db->where('from_unixtime(pg_thrift_group_payment_recieves.thrift_group_payment_date, "%Y-%m-%d")<=', $end);
        $this->db->where('pg_thrift_group_payment_recieves.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    /*-----------------------------------------------------------------------------------------------------------------*/


    public function doesThriftGroupNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group');
        $this->db->where('thrift_group_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function doesThriftGroupPaymentNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments');
        $this->db->where('thrift_group_payment_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        /*$result_array = $query->result_array();
        return $result_array;*/

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function doesThriftGroupPaymentRecieveNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves');
        $this->db->where('thrift_group_payment_recieve_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function doesThriftGroupCombinePaymentNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves');
        $this->db->where('thrift_group_combine_payment_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function getThriftMembersIds($thrift_group_id)
    {
        $this->db->select('thrift_group_member_id');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where('tgm.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
    }

    public function increaseUsersRatings($user_ids)
    {
        $this->db->query(
            'UPDATE rspm_tbl_user_details
            SET user_rating = user_rating +1 
            WHERE user_id IN (' . implode(',', $user_ids) . ')'
        );
    }


    /*------------------------------------------------------------------------------------------------------------*/
    public function countTotalColleaguesBySelect2($my_id, $employer_id, $keyword)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id,
        ');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.id!=', $my_id);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); //employee is 7

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');
        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }

        $this->db->group_start();
        $this->db->like('u.first_name', $keyword);
        $this->db->or_like('u.last_name', $keyword);
        $this->db->or_like('u.email', $keyword);
        $this->db->group_end();

        $query = $this->db->get();

        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalColleaguesBySelect2($my_id, $employer_id, $keyword, $limit, $offset)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id,
        ');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.id!=', $my_id);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); //employee is 7

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }


        $this->db->group_start();
        $this->db->like('u.first_name', $keyword);
        $this->db->or_like('u.last_name', $keyword);
        $this->db->or_like('u.email', $keyword);
        $this->db->group_end();

        $this->db->limit($limit, $offset);

        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    public function getEmployeeColleaguesIds($my_id, $employer_id)
    {
        $this->db->select('
                        u.id as id,                      
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id,
        ');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.id!=', $my_id);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); //employee is 7

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');
        $this->db->where('ud.user_employer_id', $employer_id);


        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }


    public function insertCustomProductInvitation($ins_data)
    {
        $this->db->insert('pg_custom_product_invitation', $ins_data);

        return $this->db->insert_id();
    }

    public function insertCustomProductInvitedMembers($ins_data)
    {
        $this->db->insert('pg_custom_product_invited_members', $ins_data);
    }


    /*----------------------------------------------------------------------------------------------------------------*/
    public function countSentProductCreationList($common_filter_value = false, $specific_filters = false, $creator_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as pi');
        $this->db->join('pg_thrift_group as tg', 'pi.cpi_thrift_group_id=tg.thrift_group_id');

        if ($creator_id) {
            $this->db->where('pi.cpi_created_by', $creator_id);
        }


        if ($common_filter_value != false) {

            $this->db->like('tg.thrift_group_number', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'cpi_number') {
                    $this->db->like('pi.' . $column_name, $filter_value);
                }


            }


        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getSentProductCreationList($common_filter_value = false, $specific_filters = false, $order, $limit, $creator_id)
    {

        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as pi');
        $this->db->join('pg_thrift_group as tg', 'pi.cpi_thrift_group_id=tg.thrift_group_id');

        if ($creator_id) {
            $this->db->where('pi.cpi_created_by=', $creator_id);
        }


        if ($common_filter_value != false) {

            $this->db->like('tg.thrift_group_number', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'cpi_number') {
                    $this->db->like('pi.' . $column_name, $filter_value);
                }


            }


        }


        $this->db->order_by('pi.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();

        $result = $query->result();


        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function countRecievedProductCreationList($common_filter_value = false, $specific_filters = false, $reciever_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as pi');

        $this->db->join('pg_custom_product_invited_members as pim', 'pi.cpi_id=pim.cpi_id');

        if ($reciever_id) {
            $this->db->where('pim.cpi_inv_to=', $reciever_id);
        }

        $this->db->where('pim.cpi_is_invitor !=', 1);

        if ($common_filter_value != false) {

            $this->db->like('pi.cpi_number', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'cpi_number') {
                    $this->db->like('pi.' . $column_name, $filter_value);
                }

                if ($column_name == 'cpi_inv_accepted') {
                    if ($filter_value == 'accepted') {
                        $this->db->where('pim.cpi_inv_accepted', 1);
                    } else if ($filter_value == 'pending') {
                        $this->db->where('pim.cpi_inv_accepted', 0);
                    } else if ($filter_value == 'declined') {
                        $this->db->where('pim.cpi_inv_accepted', -1);
                    }

                }


            }


        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getRecievedProductCreationList($common_filter_value = false, $specific_filters = false, $order, $limit, $reciever_id)
    {

        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as pi');

        $this->db->join('pg_custom_product_invited_members as pim', 'pi.cpi_id=pim.cpi_id');

        if ($reciever_id) {
            $this->db->where('pim.cpi_inv_to=', $reciever_id);
        }

        $this->db->where('pim.cpi_is_invitor !=', 1);

        if ($common_filter_value != false) {

            $this->db->like('pi.cpi_number', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'cpi_number') {
                    $this->db->like('pi.' . $column_name, $filter_value);
                }

                if ($column_name == 'cpi_inv_accepted') {
                    if ($filter_value == 'accepted') {
                        $this->db->where('pim.cpi_inv_accepted', 1);
                    } else if ($filter_value == 'pending') {
                        $this->db->where('pim.cpi_inv_accepted', 0);
                    } else if ($filter_value == 'declined') {
                        $this->db->where('pim.cpi_inv_accepted', -1);
                    }

                }


            }


        }


        $this->db->order_by('pi.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();

        $result = $query->result();


        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function countCpiSentTo($cpi_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->where('cpim.cpi_id', $cpi_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countCpiAcceptedBy($cpi_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->where('cpim.cpi_id', $cpi_id);
        $this->db->where('cpim.cpi_inv_accepted', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countCpiPending($cpi_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->where('cpim.cpi_id', $cpi_id);
        $this->db->where('cpim.cpi_inv_accepted', 0);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function getCustomProductInvitation($cpi_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as cpi');
        $this->db->where('cpi.cpi_id', $cpi_id);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }


    public function getCustomProductInvitationByThriftGroup($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as cpi');
        $this->db->where('cpi.cpi_thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }


    public function AcceptCustomProduct($cpi_id, $reciever_id, $curr_ts)
    {
        $this->db->set('cpi_inv_accepted', 1);
        $this->db->set('cpi_inv_accepted_at', $curr_ts);
        $this->db->where('cpi_id', $cpi_id);
        $this->db->where('cpi_inv_to', $reciever_id);
        $this->db->update('pg_custom_product_invited_members');

        return true;
    }

    public function DeclineCustomProduct($cpi_id, $reciever_id)
    {
        $this->db->set('cpi_inv_accepted', -1);
        $this->db->where('cpi_id', $cpi_id);
        $this->db->where('cpi_inv_to', $reciever_id);
        $this->db->update('pg_custom_product_invited_members');

        return true;
    }

    public function getCustomProductInvitationMembers($cpi_id, $only_accepted_members, $only_declined_members)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->where('cpim.cpi_id', $cpi_id);

        if ($only_accepted_members && !$only_declined_members) {
            $this->db->where('cpim.cpi_inv_accepted', 1);
        }

        if (!$only_accepted_members && $only_declined_members) {
            $this->db->where('cpim.cpi_inv_accepted', -1);
        }

        $this->db->order_by('cpim.cpi_inv_order', 'asc');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }


    public function checkThriftViewPermission($thrift_group_id, $which_list, $user_id)
    {
        $this->db->select('tg.thrift_group_id,', false);


        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_id', $thrift_group_id);

        /*extra starts*/
        $my_query = 'Select u.id from users as u where u.deletion_status!=1 and u.id=' . $user_id;

        $my_employees_query =
            'Select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $user_id . ')';


        if ($which_list == 'my' || $which_list == 'my_employees') {

            $extra_user_query = '';

            if ($which_list == 'my') {
                $extra_query = $my_query;
            }

            if ($which_list == 'my_employees') {
                $extra_query = $my_employees_query;
            }

            $thrifts_query =
                'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $extra_query . ')';


            $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);
        }
        /*extra ends*/


        $query = $this->db->get();

        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getTimeCrossedCustomThriftGroups($curr_ts)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        //$this->db->group_start();
        $this->db->where('tg.thrift_group_open', 1);
        $this->db->where('tg.thrift_group_is_custom_product', 1);
        $this->db->where('tg.thrift_group_start_date<', $curr_ts);
        //$this->db->group_end();

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getCustomProductAcceptedMembers($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->join('pg_custom_product_invitation as cpi', 'cpim.cpi_id=cpi.cpi_id', 'left');

        $this->db->where('cpi.cpi_thrift_group_id', $thrift_group_id);
        $this->db->where('cpim.cpi_inv_accepted', 1);

        $this->db->order_by('cpim.cpi_inv_order', 'asc');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getCustomProductInvitaionWithMembers($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->join('pg_custom_product_invitation as cpi', 'cpim.cpi_id=cpi.cpi_id', 'left');

        $this->db->where('cpi.cpi_thrift_group_id', $thrift_group_id);

        $this->db->order_by('cpim.cpi_inv_accepted_at', 'asc');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function insertMessage($ins_data)
    {
        $this->db->insert('pg_message', $ins_data);

        return $this->db->insert_id();
    }

    public function insertMessageReciever($ins_data)
    {
        $this->db->insert('pg_message_reciever', $ins_data);
    }

    public function insertMessageComment($ins_data)
    {
        $this->db->insert('pg_message_comment', $ins_data);

        return $this->db->insert_id();
    }

    public function doesMessageNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_message');
        $this->db->where('message_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function deleteThriftGroup($thrift_group_id)
    {
        $this->db->where('thrift_group_id', $thrift_group_id);
        $this->db->delete('pg_thrift_group');
    }

    public function deleteThriftGroupMembers($thrift_group_id)
    {
        $this->db->where('thrift_group_id', $thrift_group_id);
        $this->db->delete('pg_thrift_group_members');
    }

    public function deleteInvitation($cpi_id)
    {
        $this->db->where('cpi_id', $cpi_id);
        $this->db->delete('pg_custom_product_invitation');
    }

    public function deleteInvitedMembers($cpi_id)
    {
        $this->db->where('cpi_id', $cpi_id);
        $this->db->delete('pg_custom_product_invited_members');
    }

    public function getInvitaionByCpiId($cpi_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation');
        $this->db->where('cpi_id', $cpi_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;

    }

    public function getInvitaionByThriftGroupId($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation');
        $this->db->where('cpi_thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;

    }

    public function getEmailTempltateByType($type)
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        $this->db->where('email_template_type', $type);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getUsers($id_arr)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where_in('id', $id_arr);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getBank($bank_id)
    {
        $this->db->select('*');
        $this->db->from('pg_bank');
        $this->db->where('bank_id', $bank_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function updateUsersPaymentInfo($upd_data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $upd_data);
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getUserByPayStackCustomerCode($paystack_customer_code)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $this->db->where('ud.paystack_customer_code', $paystack_customer_code);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getThriftGroupByPlanCode($paystack_plan_code)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.paystack_plan_code', $paystack_plan_code);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getLastPayStackPaymentByGroupAndMember($thrift_group_id, $thrift_group_member_id)
    {
        $this->db->select('*');
        $this->db->from('paystack_payment_hook as ph');
        $this->db->where('ph.thrift_group_id', $thrift_group_id);
        $this->db->where('ph.thrift_group_payer_member_id', $thrift_group_member_id);

        $this->db->order_by('paystack_payment_cycle_number', 'desc');
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();

        $ret = null;

        if ($result) {
            if ($result[0]) {
                $ret = $result[0];
            }
        }
        return $ret;
    }

    public function insertPaystackPaymentHook($ins_data)
    {
        $this->db->insert('paystack_payment_hook', $ins_data);
        return $this->db->insert_id();
    }

    public function getPaystackRelatedPayment($thrift_group_id, $thrift_group_member_id, $thrift_group_payment_cycle_number)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_id', $thrift_group_id);
        $this->db->where('tp.thrift_group_payer_member_id', $thrift_group_member_id);
        $this->db->where('tp.thrift_group_payment_cycle_number', $thrift_group_payment_cycle_number);
        $this->db->where('tp.eligible_for_paystack_charge', 1);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getPayment($payment_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_payment_id', $payment_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getPaymentRecieve($payment_recieve_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->where('tr.thrift_group_payment_recieve_id', $payment_recieve_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updatePaystackPaymentHook($upd_data, $serial)
    {
        $this->db->where('paystack_payment_hook_serial', $serial);
        $this->db->update('paystack_payment_hook', $upd_data);
    }

    public function getEligiblePaymentsForRemainders($reminder_time, $reminder_num)
    {
        $current_timestamp = $this->custom_datetime_library->getCurrentTimestamp();
        $target_time = $current_timestamp - $reminder_time;

        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tgp');
        //$this->db->where('tgp.thrift_group_is_payment_paid!=', 1);
        $this->db->where('tgp.thrift_group_payment_date <', $target_time);

        if ($reminder_num == 1) {
            $this->db->where('tgp.thrift_group_reminder_email_count ', 0);
        }

        if ($reminder_num == 2) {
            $this->db->where('tgp.thrift_group_reminder_email_count ', 1);
        }
        $this->db->limit(50);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getEligiblePaymentsForDeclined($curr_ts_minus_extratime)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tgp');
        $this->db->where('tgp.thrift_group_is_payment_paid', 1);
        $this->db->where('tgp.payment_solution_status!=', 1);
        $this->db->where('tgp.eligible_for_paystack_charge', 1);
        $this->db->where('tgp.thrift_group_payment_date <', $curr_ts_minus_extratime);
        $this->db->where('tgp.thrift_group_payment_declined_email_count <', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function mockPayment($pid)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tgp');
        $this->db->where('tgp.thrift_group_payment_id', $pid);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function getEligiblePaymentRecievesForRemainders($reminder_time, $reminder_num)
    {
        $current_timestamp = $this->custom_datetime_library->getCurrentTimestamp();
        $target_time = $current_timestamp - $reminder_time;

        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tgr');
        //$this->db->where('tgr.thrift_group_is_payment_recieved!=', 1);
        $this->db->where('tgr.thrift_group_payment_date <', $target_time);

        if ($reminder_num == 1) {
            $this->db->where('tgr.thrift_group_reminder_email_count ', 0);
        }

        if ($reminder_num == 2) {
            $this->db->where('tgr.thrift_group_reminder_email_count ', 1);
        }

        $this->db->limit(50);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function countPaymentIssue($common_filter_value = false, $specific_filters = false, $which_issue, $employer_id, $employee_id, $date_from, $date_to,$curr_ts,$extended_time)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tp.thrift_group_payment_id,
                       tp.thrift_group_combine_payment_number,
                       tp.thrift_group_payment_amount,
                       tp.thrift_group_payer_member_id,
                       tp.thrift_group_payment_number,
                       tp.thrift_group_payee_member_id,
                       tp.thrift_group_payment_date,
                       tp.payment_solution_status,
                       tp.eligible_for_paystack_charge,
                       tp.paystack_payment_cleared,
                       tp.paystack_payment_reference,                        	 	
                       u.first_name,
                       u.last_name,
                       u.email
                        
        ', false);

        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id', 'left');
        $this->db->join('users as u', 'tp.thrift_group_payer_member_id=u.id');


        if ($date_from && $date_to) {
            $this->db->where('tp.thrift_group_payment_date >=', $date_from);
            $this->db->where('tp.thrift_group_payment_date <=', $date_to);
        }

        if (($which_issue == 'employee_issue' || $which_issue == 'employee_issue_as_employee')
            && $employee_id
        ) {
            $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        }

        if (($which_issue == 'employer_issue' || $which_issue == 'employer_issue_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tp .thrift_group_payer_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tp.thrift_group_payment_number', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('concat(u.first_name," ",u.last_name)', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_number') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_amount') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like('concat(u.first_name," ",u.last_name)', $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'email') {
                    $this->db->like('u.email', $filter_value);
                }

                if ($column_name == 'thrift_group_payer_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payer_member_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_payee_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payee_member_id', $filter_value);
                    }

                }

                if ($column_name == 'status') {
                    if ($filter_value == 'all' || $filter_value == '') {
                        //do nothing
                    } else if ($filter_value == 'scheduled') {
                        $this->db->where('tp.thrift_group_payment_date >', $curr_ts);
                    } else if ($filter_value == 'waiting') {
                        $this->db->where('tp.thrift_group_payment_date <', $curr_ts);
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} >", $curr_ts,false);
                    } else if ($filter_value == 'unsuccessful') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared !=", 1);
                        $this->db->where("tp.payment_solution_status !=", 1);
                    } else if ($filter_value == 'solved') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared !=", 1);
                        $this->db->where("tp.payment_solution_status", 1);
                    } else if ($filter_value == 'successful') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared", 1);
                    } else if ($filter_value == 'complete') {
                        $this->db->where("tp.thrift_group_payment_date <", $curr_ts);
                        $this->db->where("tp.eligible_for_paystack_charge", 0);
                    }
                }


            }

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


    public function getPaymentIssue($common_filter_value = false, $specific_filters = false, $order, $limit, $which_issue, $employer_id, $employee_id, $date_from, $date_to,$curr_ts,$extended_time)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tp.thrift_group_payment_id,
                       tp.thrift_group_combine_payment_number,
                       tp.thrift_group_payment_amount,
                       tp.thrift_group_payer_member_id,
                       tp.thrift_group_payment_number,
                       tp.thrift_group_payee_member_id,
                       tp.thrift_group_payment_date,
                       tp.payment_solution_status,
                       tp.eligible_for_paystack_charge,
                       tp.paystack_payment_cleared,
                       tp.paystack_payment_reference, 
                       u.first_name,
                       u.last_name,
                       u.email
                        
        ', false);

        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id', 'left');
        $this->db->join('users as u', 'tp.thrift_group_payer_member_id=u.id');

        if ($date_from && $date_to) {
            $this->db->where('tp.thrift_group_payment_date >=', $date_from);
            $this->db->where('tp.thrift_group_payment_date <=', $date_to);
        }

        if (($which_issue == 'employee_issue' || $which_issue == 'employee_issue_as_employee')
            && $employee_id
        ) {
            $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        }

        if (($which_issue == 'employer_issue' || $which_issue == 'employer_issue_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tp.thrift_group_payer_member_id', $employee_query, false);

        }

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tp.thrift_group_payment_number', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('concat(u.first_name," ",u.last_name)', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_number') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_amount') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like('concat(u.first_name," ",u.last_name)', $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'email') {
                    $this->db->like('u.email', $filter_value);
                }

                if ($column_name == 'thrift_group_payer_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payer_member_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_payee_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payee_member_id', $filter_value);
                    }

                }

                if ($column_name == 'status') {
                    if ($filter_value == 'all' || $filter_value == '') {
                        //do nothing
                    } else if ($filter_value == 'scheduled') {
                        $this->db->where('tp.thrift_group_payment_date >', $curr_ts);
                    } else if ($filter_value == 'waiting') {
                        $this->db->where('tp.thrift_group_payment_date <', $curr_ts);
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} >", $curr_ts,false);
                    } else if ($filter_value == 'unsuccessful') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared !=", 1);
                        $this->db->where("tp.payment_solution_status !=", 1);
                    } else if ($filter_value == 'solved') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared !=", 1);
                        $this->db->where("tp.payment_solution_status", 1);
                    } else if ($filter_value == 'successful') {
                        $this->db->where("tp.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tp.eligible_for_paystack_charge", 1);
                        $this->db->where("tp.paystack_payment_cleared", 1);
                    } else if ($filter_value == 'complete') {
                        $this->db->where("tp.thrift_group_payment_date <", $curr_ts);
                        $this->db->where("tp.eligible_for_paystack_charge", 0);
                    }
                }


            }

        }


        if ($order['column'] == 'thrift_group_number'
            ||
            $order['column'] == 'thrift_group_start_date'
            ||
            $order['column'] == 'thrift_group_end_date'
        ) {
            $this->db->order_by('tg.' . $order['column'], $order['by']);
        }

        if ($order['column'] == 'thrift_group_payment_amount'
            ||
            $order['column'] == 'thrift_group_payment_number'
            ||
            $order['column'] == 'thrift_group_payment_date'
        ) {
            $this->db->order_by('tp.' . $order['column'], $order['by']);
        }


        if ($limit) {
            $this->db->limit($limit['length'], $limit['start']);
        }


        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    /*------------------------------------------*/

    public function countPaymentRecieveIssue($common_filter_value = false, $specific_filters = false, $which_issue, $employer_id, $employee_id, $date_from, $date_to, $curr_ts, $extended_time)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tr.thrift_group_payment_recieve_id,
                       tr.thrift_group_combine_payment_number,
                       tr.thrift_group_payment_recieve_amount,
                       tr.thrift_group_member_id,
                       tr.thrift_group_payment_recieve_number,                      
                       tr.thrift_group_payment_date,                       
                       tr.payment_solution_status,
                       tr.eligible_for_paystack_payment_transfer,
                       tr.paystack_payment_transferred,
                       tr.paystack_payment_transfer_error_object,
                       tr.paystack_payment_transfer_error_message,                       
                       u.first_name,
                       u.last_name,
                       u.email
                        
        ', false);

        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id', 'left');
        $this->db->join('users as u', 'tr.thrift_group_member_id=u.id');


        if ($date_from && $date_to) {
            $this->db->where('tr.thrift_group_payment_date >=', $date_from);
            $this->db->where('tr.thrift_group_payment_date <=', $date_to);
        }

        if (($which_issue == 'employee_issue' || $which_issue == 'employee_issue_as_employee')
            && $employee_id
        ) {
            $this->db->where('tr.thrift_group_member_id', $employee_id);
        }

        if (($which_issue == 'employer_issue' || $which_issue == 'employer_issue_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tr .thrift_group_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tr.thrift_group_payment_recieve_number', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('concat(u.first_name," ",u.last_name)', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like('concat(u.first_name," ",u.last_name)', $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'email') {
                    $this->db->like('u.email', $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_number') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_amount') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tr.thrift_group_member_id', $filter_value);
                    }

                }

                if ($column_name == 'status') {
                    if ($filter_value == 'all' || $filter_value == '') {
                        //do nothing
                    } else if ($filter_value == 'scheduled') {
                        $this->db->where('tr.thrift_group_payment_date >', $curr_ts);
                    } else if ($filter_value == 'waiting') {
                        $this->db->where('tr.thrift_group_payment_date <', $curr_ts);
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} >", $curr_ts,false);
                    } else if ($filter_value == 'unsuccessful') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", -1);
                        $this->db->where("tr.payment_solution_status!=", 1);
                    } else if ($filter_value == 'solved') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", -1);
                        $this->db->where("tr.payment_solution_status", 1);
                    } else if ($filter_value == 'successful') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", 1);
                    } else if ($filter_value == 'complete') {
                        $this->db->where("tr.thrift_group_payment_date <", $curr_ts);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 0);
                    }
                }


            }

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


    public function getPaymentRecieveIssue($common_filter_value = false, $specific_filters = false, $order, $limit, $which_issue, $employer_id, $employee_id, $date_from, $date_to, $curr_ts, $extended_time)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tr.thrift_group_payment_recieve_id,
                       tr.thrift_group_combine_payment_number,
                       tr.thrift_group_payment_recieve_amount,
                       tr.thrift_group_member_id,
                       tr.thrift_group_payment_recieve_number,                      
                       tr.thrift_group_payment_date,
                       tr.payment_solution_status,
                       tr.eligible_for_paystack_payment_transfer,
                       tr.paystack_payment_transferred,
                       tr.paystack_payment_transfer_error_object,
                       tr.paystack_payment_transfer_error_message,
                       u.first_name,
                       u.last_name,
                       u.email
                        
        ', false);

        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id', 'left');
        $this->db->join('users as u', 'tr.thrift_group_member_id=u.id');


        if ($date_from && $date_to) {
            $this->db->where('tr.thrift_group_payment_date >=', $date_from);
            $this->db->where('tr.thrift_group_payment_date <=', $date_to);
        }

        if (($which_issue == 'employee_issue' || $which_issue == 'employee_issue_as_employee')
            && $employee_id
        ) {
            $this->db->where('tr.thrift_group_member_id', $employee_id);
        }

        if (($which_issue == 'employer_issue' || $which_issue == 'employer_issue_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tr .thrift_group_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tr.thrift_group_payment_recieve_number', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('concat(u.first_name," ",u.last_name)', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like('concat(u.first_name," ",u.last_name)', $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'email') {
                    $this->db->like('u.email', $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_number') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_amount') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tr.thrift_group_member_id', $filter_value);
                    }

                }

                if ($column_name == 'status') {
                    if ($filter_value == 'all' || $filter_value == '') {
                        //do nothing
                    } else if ($filter_value == 'scheduled') {
                        $this->db->where('tr.thrift_group_payment_date >', $curr_ts);
                    } else if ($filter_value == 'waiting') {
                        $this->db->where('tr.thrift_group_payment_date <', $curr_ts);
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} >", $curr_ts,false);
                    } else if ($filter_value == 'unsuccessful') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", -1);
                        $this->db->where("tr.payment_solution_status!=", 1);
                    } else if ($filter_value == 'solved') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", -1);
                        $this->db->where("tr.payment_solution_status", 1);
                    } else if ($filter_value == 'successful') {
                        $this->db->where("tr.thrift_group_payment_date+{$extended_time} <", $curr_ts,false);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 1);
                        $this->db->where("tr.paystack_payment_transferred", 1);
                    } else if ($filter_value == 'complete') {
                        $this->db->where("tr.thrift_group_payment_date <", $curr_ts);
                        $this->db->where("tr.eligible_for_paystack_payment_transfer", 0);
                    }
                }


            }

        }


        if ($order['column'] == 'thrift_group_number'
            ||
            $order['column'] == 'thrift_group_start_date'
            ||
            $order['column'] == 'thrift_group_end_date'
        ) {
            $this->db->order_by('tg.' . $order['column'], $order['by']);
        }

        if ($order['column'] == 'thrift_group_payment_recieve_amount'
            ||
            $order['column'] == 'thrift_group_payment_recieve_number'
            ||
            $order['column'] == 'thrift_group_payment_date'
        ) {
            $this->db->order_by('tr.' . $order['column'], $order['by']);
        }


        if ($limit) {
            $this->db->limit($limit['length'], $limit['start']);
        }


        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    public function getActiveNonBotAdmins()
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');

        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->where('u.is_user_bot', 0);
        $this->db->where('u.deletion_status !=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id =', 1);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $result = $query->result();
        return $result;
    }

    public function ifEmailExists($email)
    {
        $ret = false;
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.email', $email);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            $ret = true;
        }

        return $ret;
    }

    public function getSingleInvitedMember($cpi_inv_mem_serial)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');
        $this->db->where('cpim.cpi_inv_mem_serial', $cpi_inv_mem_serial);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function decreseInvitationOrderByOne($cpi_id, $cpi_inv_order)
    {
        $this->db->set('cpi_inv_order', 'cpi_inv_order-1', FALSE);
        $this->db->where('cpi_id', $cpi_id);
        $this->db->where('cpi_inv_order>', $cpi_inv_order);
        $this->db->update('pg_custom_product_invited_members');
    }

    public function removeInvitedMember($cpi_inv_mem_serial)
    {
        $this->db->where('cpi_inv_mem_serial', $cpi_inv_mem_serial);
        $this->db->delete('pg_custom_product_invited_members');
    }

    public function invitationOrderUp($cpi_inv_mem_serial)
    {
        $this->db->set('cpi_inv_order', 'cpi_inv_order-1', FALSE);
        $this->db->where('cpi_inv_mem_serial', $cpi_inv_mem_serial);
        $this->db->update('pg_custom_product_invited_members');
    }

    public function invitationOrderDown($cpi_inv_mem_serial)
    {
        $this->db->set('cpi_inv_order', 'cpi_inv_order+1', FALSE);
        $this->db->where('cpi_inv_mem_serial', $cpi_inv_mem_serial);
        $this->db->update('pg_custom_product_invited_members');
    }

    public function getCustomProductInvitor($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as cpi');
        $this->db->join('pg_custom_product_invited_members as cpim', 'cpi.cpi_id=cpim.cpi_id');

        $this->db->where('cpi.cpi_thrift_group_id', $thrift_group_id);
        $this->db->where('cpim.cpi_is_invitor', 1);


        $query = $this->db->get();
        $row = $query->row();
        return $row;

    }


}