<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">

                <?php if ($which_form == 'add') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_add_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'edit') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_edit_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'view') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_view_text') ?></h4>
                <?php } ?>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->





    <?php if ($this->session->flashdata('successful')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('successful_text') ?></strong>
            <?php

            if ($this->session->flashdata('individual_product_create_success')) {
                echo lang('individual_product_create_success');
            }

            if ($this->session->flashdata('individual_product_update_success')) {
                echo lang('individual_product_update_success');
            }
            ?>

            <?php if ($this->session->flashdata('flash_thrift_group_id_id')) { ?>
                <a href="<?php echo base_url() . 'thrift_module/view_thrift/' . $this->session->flashdata('flash_thrift_group_id_id') ?>">
                    <?php echo lang('view_individual_product_thrift_text'); ?>
                </a>
            <?php } ?>

        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('unsuccessful')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }
            ?>

            <?php if ($this->session->flashdata('flash_thrift_percentage_error')) { ?>
                <?php echo sprintf($this->lang->line('flash_thrift_percentage_error_text'), $this->session->flashdata('flash_thrift_percentage_error')); ?>
            <?php } ?>

        </div>
    <?php } ?>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">
                    <?php if ($which_form == 'add') { ?>
                        <?= lang('box_title_add_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'edit') { ?>
                        <?= lang('box_title_edit_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'view') { ?>
                        <?= lang('box_title_view_text') ?>
                    <?php } ?>
                </h4>


                <div class="row">
                    <div class="col-md-12">
                        <form id="#individual_product_form" action="<?= $form_action ?>" method="post"
                              enctype="multipart/form-data">


                            <fieldset class="form-group">
                                <label><?= lang('thrift_amount_text') ?> <?= lang('per_person_per_month_text') ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><?= $currency_sign ?> </span>
                                    <input <?php if ($which_form == 'view') { ?> readonly="readonly" <?php } ?>
                                            name="product_price" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('thrift_amount_text') ?>"
                                            type="number"
                                            min="0.01" step="0.01"
                                            required
                                            value="<?= $this->session->flashdata('flash_product_price') ? $this->session->flashdata('flash_product_price') : '' ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('number_of_payments_text') ?> </label>
                                <div class="input-group">

                                    <input <?php if ($which_form == 'view') { ?> readonly="readonly" <?php } ?>
                                            name="number_of_payments" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('number_of_payments_text') ?>"
                                            type="number"
                                            min="1" step="1"
                                            required
                                            value="<?= $this->session->flashdata('flash_number_of_payments') ? $this->session->flashdata('flash_number_of_payments') : '' ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('set_start_date_text') ?></label>
                                <div>
                                    <div class="input-group">
                                        <span class="input-group-addon bg-custom b-0"><i
                                                    class="icon-calender"></i></span>
                                        <input readonly="readonly" type="text" name="start_date" class="form-control"
                                               placeholder="<?= lang('set_start_date_text') ?>" id="start_date"
                                               required
                                               value="<?= $this->session->flashdata('flash_start_date') ? $this->session->flashdata('flash_start_date') : '' ?>"
                                        >

                                    </div><!-- input-group -->
                                </div>
                            </fieldset>

                            <?php if ($which_form == 'add') { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_create_thrift_text') ?></button>
                            <?php } ?>

                            <?php if ($which_form == 'edit') { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_update_text') ?></button>
                            <?php } ?>

                        </form>
                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>


</div> <!-- container -->


<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<script>
    $(function () {


        <?php if($which_form != 'view') { ?>

        $('#start_date').datepicker({
            autoclose: true,
            disableEntry: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            startDate: "<?= $individual_thrift_start_delay?>",
            endDate: "<?= $individual_thrift_end_delay?>"
        });

        <?php } ?>


    });
</script>
