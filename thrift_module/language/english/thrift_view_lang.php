<?php


$lang['page_title_thrift_details_page_text'] = 'Thrift Details page';
$lang['breadcrum_thrift_text'] = 'Thrift';
$lang['breadcrum_thrift_details_text'] = 'Thrift Details';


$lang['key_text'] = 'Item';
$lang['val_text'] = 'Information';


/*thrift group*/
$lang['thrift_group_number_text'] = 'Thrift Group ID';

$lang['thrift_group_member_count_text'] = 'Members';
$lang['thrift_group_member_limit_text'] = 'Member Limit';

$lang['thrift_group_term_duration_text'] = 'Total Months';
$lang['thrift_group_current_cycle_text'] = 'Current Cycle';

$lang['thrift_group_activation_status_text'] = 'Active?';
$lang['thrift_group_open_text'] = 'Open?';

$lang['thrift_group_creation_date_text'] = 'Group Creation date';
$lang['thrift_group_start_date_text'] = 'Thrift Start Date';
$lang['thrift_group_end_date_text'] = 'Thrift End Date';

$lang['thrift_group_this_month_payment_text'] = 'Monthly Payment';
$lang['thrift_group_this_month_disbursement_text'] = 'Disbursement Amount';

$lang['yes_text'] = 'Yes';
$lang['no_text'] = 'No';


/*members*/
$lang['member_name_text'] = 'Member';
$lang['employer_name_text'] = 'Organization';
$lang['comany_text'] = 'Organization';
$lang['member_number_text'] = 'Member Number.';
$lang['thrift_order_text'] = 'Thrift Order';
$lang['disbursement_order_text'] = 'Disbursement Order';
$lang['member_join_date_text'] = 'Join Date';

$lang['mem_id_num_text'] = 'Member ID';

/*payments*/
$lang['payment_number_text'] = 'Payment ID';
$lang['cycle_number_text'] = 'Cycle';
$lang['payee_or_payer_text'] = 'Payee/Payer';
$lang['payment_paid_text'] = 'Payment Paid?';
$lang['payment_date_text'] = 'Payment date';

$lang['payment_amount_text'] = 'Amount';

$lang['payment_recieved_text'] = 'Payment Received?';
$lang['payment_date_text'] = 'Payment Date';
$lang['payment_status_text'] = 'Status';

$lang['thrift_details_text'] = 'Thrift Details';
$lang['member_details_text'] = 'Member Details';
$lang['payment_details_text'] = 'Payment Details';
$lang['payment_recieve_details_text'] = 'Payment Receive Details';
$lang['payments_and_disbursement_text'] = 'Payments And Disbursements';

$lang['payment_completed_text'] = 'Completed';
$lang['payment_scheduled_text'] = 'Scheduled';

$lang['payee_text'] = 'Payee';
$lang['payer_text'] = 'Payer';

$lang['unavailable_text'] = 'Unavailable';
$lang['not_started_text'] = 'Not Started';


/*--------------------------*/

$lang['invited_members_text'] = 'Invited Members';
$lang['members_text'] = 'Members';
$lang['status_text'] = 'Status';

$lang['initiator_text'] = 'Initiator';

$lang['accepted_text'] = 'Accepted';
$lang['declined_text'] = 'Declined';
$lang['pending_text'] = 'Pending';

