<?php
/*page texts*/
$lang['page_title_text'] = 'Create User';
$lang['page_subtitle_text'] = 'Add user\'s basic information';
$lang['box_title_text'] = 'User Information Form';
$lang['no_user_found_text'] = 'No User Is Found !';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Users';
$lang['breadcrumb_page_text'] = 'Create User';

/*Add user form texts*/
$lang['label_firstname_text'] = 'First Name';
$lang['label_lastame_text'] = 'Last Name';
$lang['label_email_text'] = 'Email Address';
$lang['label_phone_text'] = 'Phone Number';
$lang['label_company_name_text'] = 'Company Name';
$lang['label_password_text'] = 'Password';
$lang['label_confirm_password_text'] = 'Confirm Password';
$lang['label_select_group_text'] = 'Select Group';

$lang['select_employer_text'] = 'Select Employer';
$lang['label_select_employer_text'] = 'Select an employer';
$lang['select_employer_placeholder_text'] = 'Select an employer';
$lang['no_employer_found_text'] = 'No employer is found';

$lang['need_to_select_an_employer_text'] = 'Need to select an employer';



$lang['option_none_text'] = 'None';

$lang['placeholder_firstname_text'] = 'Enter First Name';
$lang['placeholder_lastame_text'] = 'Enter Last Name';
$lang['placeholder_email_text'] = 'Enter a valid Email Address ';
$lang['placeholder_phone_text'] = 'Enter Phone Number';
$lang['placeholder_company_name_text'] = 'Enter Company Name ';
$lang['placeholder_password_text'] = 'Enter Password';
$lang['placeholder_confirm_password_text'] = 'Confirm Entered Password';

$lang['add_an_user_text'] = 'Add A User';
$lang['button_submit_text'] = 'Create User';

/*validation error texts*/
$lang['identity_text'] = 'Username/Email';
$lang['firstname_required_text'] = 'First Name is Required';
$lang['lastname_required_text'] = 'Last Name is Required';

$lang['identity_required_text'] = 'Username/Email is rquired';
$lang['identity_not_unique_text'] = 'Username/Email already exists';

$lang['email_required_text'] = 'Email Address is required';
$lang['email_not_valid_text'] = 'Email Address is not valid';
$lang['email_not_unique_text'] = 'Email Address already exists';

$lang['password_required_text'] = 'Password is required';
$lang['password_min_length_text'] = 'Minimum length of password is: ';
$lang['password_max_length_text'] = 'Maximum length of password is: ';
$lang['password_not_match_text'] = 'Passwords do not match';

$lang['confirm_password_required_text'] = 'Password Confirmation is required';