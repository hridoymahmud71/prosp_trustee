<?php

$lang['not_found_text'] = 'Not Found!';
$lang['not_found_title_text'] = 'The Page you are looking for is not found';
$lang['not_found_description_text'] =
    'The page may be missing or deleted . Please contact System Admin for more information';
$lang['contact_link_text'] = 'Contact Link';
$lang['contact_support_text'] = 'Contact Support';
$lang['thank_you_text'] = 'Thank You';