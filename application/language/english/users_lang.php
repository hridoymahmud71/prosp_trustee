<?php

/*page texts*/
$lang['page_title_text'] = 'Users';
$lang['page_subtitle_text'] = 'Manage Users';
$lang['table_title_text'] = 'User List';
$lang['no_user_found_text'] = 'No User Is Found!';
$lang['no_matching_user_found_text'] = 'No matching User Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Users';
$lang['breadcrumb_page_text'] = 'Admin';

$lang['add_button_text'] = 'Add A User';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_all_text'] = 'All';
$lang['option_admin_text'] = 'Admin';
$lang['option_staff_text'] = 'Staff';
$lang['option_client_text'] = 'Client';

$lang['column_firstname_text'] = 'First Name';
$lang['column_lastname_text'] = 'Last Name';
$lang['column_email_text'] = 'Email';
$lang['column_group_text'] = 'Role';
$lang['column_status_text'] = 'Status';
$lang['column_created_on_text'] = 'Created';
$lang['column_last_login_text'] = 'Last Login';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This User?';
$lang['swal_confirm_button_text'] = 'Yes, delete this User';
$lang['swal_cancel_button_text'] = 'No, keep this user';

$lang['swal_password_title_text'] = 'Are You Sure To Reset and Send This User\'s  Password?';
$lang['swal_password_confirm_button_text'] = 'Yes';
$lang['swal_password_cancel_button_text'] = 'No';

$lang['delete_success_text'] = 'Successfully deleted the user.';

$lang['successfull_text'] = 'Successful';
$lang['unsuccessfull_text'] = 'Unsuccessful';
$lang['user_add_success_text'] = 'Successfully added user. Edit the created user to put him/her in a group.';
$lang['update_success_text'] = 'Successfully updated the user.';

$lang['password_send_success_text']='Link for password reset is sent to user via mail';
$lang['password_send_error_text']='Link for password reset could not be sent to user via mail';

$lang['activate_success_text'] = 'User Activated';
$lang['dectivate_success_text'] = 'User Deactivated';

$lang['see_user_text'] = 'See User';
$lang['edit_user_text'] = 'Edit User';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['never_logged_in_text'] = 'Never Logged In';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make User Active';
$lang['tooltip_deactivate_text'] = 'Make User Deactive';

$lang['tooltip_view_text'] = 'View User ';
$lang['tooltip_edit_text'] = 'Edit User ';
$lang['tooltip_delete_text'] = 'Delete User ';
$lang['tooltip_message_text'] = 'Mail/Message User';

$lang['tooltip_password_text'] = 'Send password';


/*loading*/
$lang['loading_text'] = 'Loading Users . . .';




