<?php

$lang['permission_denied_text'] = 'Permission Denied!';
$lang['need_permission_text'] = 'Need Permission to Access This Page';
$lang['permission_description_text'] = 'Unfortunately You are not authorized to see the following page. For any kind of queries, please contact with the SYSTEM ADMIN';
$lang['contact_link_text'] = 'Contact Link';
$lang['thank_you_text'] = 'Thank You';