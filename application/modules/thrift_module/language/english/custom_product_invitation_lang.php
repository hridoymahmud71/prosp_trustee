<?php

$lang['custom_product_invitation_subject_text'] = 'You have got an invitation jo join a thrift';

$lang['custom_product_invitation_description_text'] = 'You have got an invitation to join a thrift.';
$lang['click_link_text'] = 'Click the link below';
$lang['view_thrift_text'] = 'View Thrift';

$lang['tooltip_text'] = 'This link takes you to the thrift';