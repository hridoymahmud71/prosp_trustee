<?php

$lang['page_title_text'] = 'Private Thrifting';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Invitations Received';


$lang['box_title_text'] = 'Invitations Received';

$lang['add_custom_product_text'] = 'New Private Thrift';


//ok
$lang['successful_text'] = '';
$lang['view_text'] = 'View Thrift';


/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_pending_text'] = 'Pending';
$lang['option_accepted_text'] = 'Accepted';
$lang['option_declined_text'] = 'Declined';

$lang['column_created_at_text'] = 'Time';
$lang['column_sender_text'] = 'Sender';
$lang['column_reciever_text'] = 'Receiver';
$lang['column_status_text'] = 'Status';
$lang['column_product_price_text'] = 'Monthly Amount';

$lang['column_actions_text'] = 'Actions';

$lang['status_pending_text'] = 'Pending';
$lang['status_accept_text'] = 'Accepted';
$lang['status_decline_text'] = 'Declined';


$lang['accept_success_text'] = 'You have successfully accepted to join the thrift!';
$lang['decline_success_text'] = 'You have declined the thrift invitation';


$lang['creation_time_unknown_text'] = 'Unknown';

/*tooltip text*/
$lang['tooltip_accept_text'] = 'Accept custom product invitaion';
$lang['tooltip_decline_text'] = 'Decline custom product invitaion';

$lang['tooltip_view_text'] = 'View Custom Product ';
$lang['tooltip_edit_text'] = 'Edit Custom Product ';
$lang['tooltip_delete_text'] = 'Delete Custom Product ';

/*loading*/
$lang['loading_text'] = 'Loading . . .';


