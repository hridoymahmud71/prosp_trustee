<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    body {
        font: 12px 'Open Sans';
    }

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .img-preview {
        max-width: 50%;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-page"> -->
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">
                        <?php echo lang($title) ?>
                        <!-- <small><?php echo lang($subtitle) ?></small> -->
                    </h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a
                                    href="product_module/add_product_info"><?php echo lang('breadcrum_home_text') ?></a>
                        </li>
                        <li class="breadcrumb-item active"><?php echo lang($breadcrumb) ?></li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('product_add_form_header_text') ?></h4>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">

                            <?php if ($this->session->flashdata('error')) { ?>

                                <?php if ($this->session->flashdata('product_insert_error')) { ?>
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('product_insert_error'); ?>
                                    </div>
                                <?php } ?>

                            <?php } ?>

                            <?php if ($this->session->flashdata('product_insert_sucess')) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('product_insert_sucess'); ?>
                                </div>
                            <?php } ?>

                            <form action="<?php echo $url; ?>" method="post" enctype="multipart/form-data">
                                <fieldset class="form-group">
                                    <label for="file_input"><?php echo lang('product_input_name_text') ?></label>
                                    <input type="text" name="product_name" value="<?= $product_name; ?>"
                                           class="form-control"
                                           placeholder="<?php echo lang('product_input_name_placeholder_text') ?>">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="file_input"><?php echo lang('product_input_number_text') ?></label>
                                    <input type="hidden" name="old_product_term_duration" value="<?= $product_term_duration; ?>">
                                    <input type="text" name="product_term_duration"
                                           value="<?= $product_term_duration; ?>" class="form-control"
                                           placeholder="<?php echo lang('product_input_number_placeholder_text') ?>">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label><?php echo lang('product_input_amount_text') ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><?php echo $this->custom_settings_library->getASettingsValue('currency_settings', 'currency_sign') ?></span>
                                        <input type="hidden" name="old_product_price" value="<?= $product_price; ?>">
                                        <input type="text" name="product_price" value="<?= $product_price; ?>"
                                               class="form-control"
                                               placeholder="<?php echo lang('product_input_amount_placeholder_text') ?>">
                                    </div>
                                </fieldset>
                                <?php $image_found = false; ?>
                                <div class="form-group">
                                    <label for="site_logo"><?php echo lang('label_site_logo_text') ?></label>
                                    <div class="main-img-preview">
                                        <?php if ($product_image) { ?>
                                            <div><?php $image_found = true ?></div>
                                            <img class="thumbnail img-preview"
                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'product/' . $product_image; ?>"
                                                 title="Preview Logo">
                                        <?php } ?>
                                        <?php if ($image_found == false) { ?>
                                            <img class="thumbnail img-preview"
                                                 src="<?php echo base_url() . 'base_demo_images/placeholder_image_demo.png' ?>"
                                                 title="Preview Logo">
                                        <?php } ?>
                                    </div>
                                    <div class="input-group">
                                        <input id="fakeUploadLogo" class="form-control fake-shadow"
                                               placeholder="Choose File" disabled="disabled">
                                        <div class="input-group-btn">
                                            <div class="fileUpload btn btn-primary fake-shadow">
                                                <span><i class="glyphicon glyphicon-upload"></i><?php echo lang('upload_site_logo_text') ?></span>
                                                <input id="logo-id" name="product_image" type="file"
                                                       value="<?= $product_image; ?>" class="attachment_upload">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <p class="help-block"><?php echo lang('help_site_logo_text') ?></p> -->
                                </div>
                                <fieldset class="form-group">
                                    <label for="file_input"><?php echo lang('product_description_text') ?></label>
                                    <textarea name="product_description" rows="7"
                                              class="form-control"><?= $product_description; ?></textarea>
                                </fieldset>
                                <input type="hidden" name="product_image" value="<?= $product_image; ?>">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo lang('prodct_submit_text') ?></button>
                            </form>
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div>
            </div><!-- end col -->
        </div>
    </div>
    <!-- </div> -->
    <!-- </div> -->
    <!-- /.content-wrapper -->


    <script>
        $(document).ready(function () {
            var brand = document.getElementById('logo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function () {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#logo-id").change(function () {
                readURL(this);
            });
        });
    </script>