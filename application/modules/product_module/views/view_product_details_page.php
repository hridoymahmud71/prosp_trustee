<!-- <div class="content-page"> -->
    <!-- Start content -->
    <!-- <div class="content"> -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left"><?php echo lang('product_title_text') ?></h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_info"><?php echo lang('breadcrum_all_product_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrum_product_home_text') ?></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="panel-body">
                            <div class="clearfix" style="margin-bottom:-15px;">
                                <?php if($all_info[0]->product_image){?>
                                <div class="pull-left" style="max-width:20%">
                                    <div class="card">
                                        <img class="card-img-top img-fluid" src="<?php echo $this->config->item('pg_upload_source_path').'/product/'.$all_info[0]->product_image;?>" alt="Product Image">
                                    </div>
                                </div>
                                <?php }?>
                                <div class="text-center">
                                    <h2 class="page-title"><?=$all_info[0]->product_name;?></h2>
                                </div>
                            </div>
                            <div class="clearfix" <?php if($all_info[0]->product_image){?> style="margin-left: 21%;" <?php }?>>
                                <div class="float-left">
                                    <h6><?php echo lang('product_thrift_duration_text')?> # 
                                        <span><b><?=$all_info[0]->product_term_duration;?> <?php echo lang('product_thrift_time_style_text')?></b></span>
                                    </h6>
                                </div>
                                <div class="float-right">
                                    <h6><?php echo lang('product_thrift_amount_text')?> #
                                        <b><?=$all_info[0]->product_price;?> <?php echo lang('product_thrift_currency_text')?></b>
                                    </h6>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <div class="clearfix">
                                        <div class="col-md-6 float-left">
                                            <h6 class=" text-inverse font-600"><b>PRODUCT POLICIES AND DESCRIPTION</b></h6>
                                        </div>
                                        <?php if($this->ion_auth->in_group('employee')){?>
                                        <div class="col-md-6 float-right">
                                            <h5 class="float-right text-inverse font-600"><a href="thrift_module/start_thrift/<?=$all_info[0]->product_id;?>"><span class="label label-success">Join the Thrift</span></a></h5>
                                        </div>
                                        <?php }?>
                                    </div>
                                    <div>
                                        All accounts are to be paid within 7 days from receipt of
                                        invoice. To be paid by cheque or credit card or direct payment
                                        online. If account is not paid within 7 days the credits details
                                        supplied as confirmation of work undertaken will be charged the
                                        agreed quoted fee noted above.
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
<!-- </div> -->