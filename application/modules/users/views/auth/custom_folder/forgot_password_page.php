<base href="<?php echo base_url();?>">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/custom_asset/prosperis_favicon.ico">
        <!-- App title -->
        <title><?php echo lang('forgot_password_text');?></title>
        <!-- Bootstrap CSS -->
        <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- App CSS -->
        <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css">
        <!-- Modernizr js -->
        <script src="assets/backend_assets/js/modernizr.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo base_url().'style_module/load_style/trustee' ?>">
    </head>
    <body>
        <div class="account-pages custom_login_backview"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page custom-wrapper-page">
            <div class="account-bg">
                <div class="card-box mb-0">
                    <div class="text-center m-t-20">
                        <div class="text-center m-t-20">
                            <a href="<?= base_url()?>" class="logo">
                                <img style="max-width: 50%;" src="<?php echo $this->config->item('pg_upload_source_path').'image/' . $site_logo;?>">
                            </a>
                        </div>
                        <?php if ($message) { ?>
                            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <?php echo $message;?>
                            </div>
                        <?php } ?>
                        <a href="index.html" class="logo">
                            <!-- <img style="max-width: 10%;" src="<?php echo $this->config->item('pg_upload_source_path').'image/' . $site_logo;?>"> -->
                        </a>
                    </div>
                    <div class="m-t-10 p-20">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6 class="text-muted text-uppercase mb-0 m-t-0"><?php echo lang('forgot_password_heading') ?></h6>
                                <p class="text-muted m-b-0 font-13 m-t-20"><?php echo lang('forgot_password_subheading') ?></p>
                            </div>
                        </div>
                        <form class="m-t-30" action="<?php echo base_url();?>users/auth/forgot_password" method="post">
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" type="email" name="identity" required="" placeholder="<?php echo lang('forgot_password_email_identity_label') ?>">
                                </div>
                            </div>
                            <div class="form-group row text-center m-t-20 mb-0">
                                <div class="col-12">
                                    <button class="btn btn-primary btn-block waves-effect waves-light" type="submit"><?php echo lang('forgot_password_submit_btn') ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end card-box-->
            <div class="m-t-20">
                <div class="text-center">
                    <p class="text-white"><?php echo lang('return_to_text');?><a href="users/auth/login" class="text-white m-l-5"><b><?php echo lang('login_text');?></b></p>
                </div>
            </div>
        </div>
        <!-- end wrapper page -->
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="assets/backend_assets/js/jquery.min.js"></script>
        <script src="assets/backend_assets/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/backend_assets/js/bootstrap.min.js"></script>
        <script src="assets/backend_assets/js/detect.js"></script>
        <script src="assets/backend_assets/js/fastclick.js"></script>
        <script src="assets/backend_assets/js/jquery.blockUI.js"></script>
        <script src="assets/backend_assets/js/waves.js"></script>
        <script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
        <script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
        <script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>
        <!-- App js -->
        <script src="assets/backend_assets/js/jquery.core.js"></script>
        <script src="assets/backend_assets/js/jquery.app.js"></script>

    
    </body>
</html>