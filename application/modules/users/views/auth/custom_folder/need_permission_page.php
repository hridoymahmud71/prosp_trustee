
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h3 class="page-title float-left">
                            <?php echo lang('permission_denied_text')?>
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"><?php echo lang('need_permission_text')?></h4>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                                <div class="box-body">
                                    <p>
                                        <?php echo lang('permission_description_text')?>
                                    </p>
                                    <p>
                                        <a href="<?php echo base_url().'contact_module/show_contact_info' ?>"><?php echo lang('contact_link_text')?></a>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <?php echo lang('thank_you_text')?>
                                </div>

                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div>
