<?php

$lang['log_page_title_text'] = 'System Logs';

$lang['today_text'] = 'Today';
$lang['yesterday_text'] = 'Yesterday';
$lang['timeline_empty_text'] = 'Nothing To Show on Timeline';

$lang['go_text'] = 'Go';

$lang['load_more_text'] = 'Load More ...';