<div class="container">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title"><?= lang('log_page_title_text') ?></h4>
        </div>
    </div>
    <!-- end row -->


    <div class="row">
        <div class="col-12">
            <div class="timeline">
                <?php if ($timeline_elements) { ?>

                    <?php foreach ($timeline_elements as $a_timeline_element) {
                        ?>
                        <article class="timeline-item log-item">
                            <div class="timeline-desk">
                                <div class="panel">
                                    <div class="timeline-box">
                                        <span class="arr"></span>
                                        <span class="timeline-icon <?php if ($a_timeline_element['is_today'] == 'today') {
                                            echo ' bg-success ';
                                        } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                                            echo ' bg-warning ';
                                        } else {
                                            echo ' bg-danger ';
                                        } ?>
                                                    ">
                                            <i class="zmdi zmdi-circle"></i>
                                        </span>
                                        <h4 class="<?php if ($a_timeline_element['is_today'] == 'today') {
                                            echo ' text-success ';
                                        } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                                            echo ' text-warning ';
                                        } else {
                                            echo ' text-danger ';
                                        } ?>
                                                    ">
                                            <?php if ($a_timeline_element['is_today'] == 'today') {
                                                echo lang('today_text');
                                            } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                                                echo lang('yesterday_text');
                                            } else {
                                                echo $a_timeline_element['log_created_at_date'];
                                            } ?>
                                        </h4>
                                        <p class="timeline-date text-muted">
                                            <small><?php echo $a_timeline_element['log_created_at_time'] ?></small>
                                        </p>
                                        <p>  <?php echo $a_timeline_element['log_title_message'] ?> </p>

                                        <?php
                                        /* if $a_timeline_element['log_type'] == %_settings ' */
                                        if (strpos($a_timeline_element['log_type'], '_settings') !== false) { ?>
                                            <hr>
                                            <p>
                                                <a href="<?php echo base_url() . $a_timeline_element['log_type_route'] ?>"
                                                   class="btn btn-primary btn-xs">
                                                    <?php echo lang('go_text') ?>
                                                </a>
                                            </p>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                <?php } ?>


                <!--<article class="timeline-item alt">
                    <div class="text-right">
                        <div class="time-show">
                            <a href="#" id="load_more_btn" class="btn btn-custom w-lg"><?/*= lang('load_more_text') */?></a>
                        </div>
                    </div>
                </article>-->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12" style="text-align: center">
            <a  href="#" id="load_more_btn" class="btn btn-custom w-lg"><?= lang('load_more_text') ?></a>
        </div>
    </div>
    <!-- end row -->

</div>

<script>
    $(function () {

        var logPage = 1;
        var itemOffset = 0;

        var totalLogs = '<?= $totalLogs?>';
        var logItemsLimit = '<?= $logItemsLimit?>';

        //alert('sss');

        init_alt_classes();

        function init_alt_classes() {

            $('.timeline-item:odd').addClass('alt');
            $('.timeline-item').find('.arr').addClass('arrow-alt');
            $('.timeline-item:even').find('.arr').addClass('arrow');
        }

        $('#load_more_btn').on('click', function (e) {

            e.preventDefault();

            items_shown = countLogs();
            console.log('items_shown: ' + items_shown + ' compare ' + 'totalLogs' + totalLogs);

            logPage++;

            itemOffset = (logPage - 1) * logItemsLimit;


            if (items_shown < totalLogs) {

                callAjax("log_module/load_more_logs");

            } else {
                $(this).prop("disabled", true);
                $(this).html('No More result');
            }

        });

        function callAjax(url) {


            $.ajax({
                type: "POST",
                dataType: 'html',
                url: url,
                data: {
                    something: 'something',
                    itemOffset: itemOffset,
                    logPage: logPage,
                    totalLogs: totalLogs,
                    logItemsLimit: logItemsLimit,
                },

                cache: false,

                beforeSend: function () {
                    state = 'beforeSend';
                    $('#load_more_btn').html('Loading');
                    setInterval(type_loading_dot, 600);
                },

                success: function (result) {

                    state = 'success';
                    //console.log(result);

                    $('#load_more_btn').html('Load More');
                    $(".timeline").append(result);
                    init_alt_classes();


                }
            });
        }

        function type_loading_dot() {

            if (state == 'beforeSend') {

                if (dots < 3) {
                    $('#load_more_btn').append('.');
                    dots++;
                }
                else {
                    $('#load_more_btn').html('Loading');
                    dots = 0;
                }

            }

        }

        function countLogs() {
            var cnt = 0;
            $(".log-item").each(function () {
                cnt++;
            });

            return cnt;
        }

    });
</script>

