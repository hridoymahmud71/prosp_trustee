<?php

/*page texts*/
$lang['page_title_text'] = 'Edit Bank Information';
$lang['breadcrum_home_text'] = 'Bank';
$lang['breadcrumb_page_add_text'] = 'Edit Bank';
$lang['page_form_title_text'] = 'Edit Bank Details';

// BANK COLUMN


$lang['column_bank_name_text'] = 'Bank Name';
$lang['column_address_text'] = 'Address';
$lang['column_website_text'] = 'Website';
$lang['column_email_text'] = 'Email';
$lang['column_phone_text'] = 'Phone';
$lang['column_status_text'] = 'Status';
$lang['column_actions_text'] = 'Action';


// add bank page

$lang['bank_name_text'] = 'Bank Name';
$lang['bank_email_text'] = 'Email';
$lang['bank_address_text'] = 'Address';
$lang['bank_website_text'] = 'Website';
$lang['bank_phone_text'] = 'Phone';
$lang['file_submit_text'] = 'Submit';
$lang['modal_cancel_text'] = 'Reset';

$lang['field_mandatory_text'] = '** This field is Required';


$lang['success_bank_upload_text'] = 'Bank Detail is Successfully Updated';
$lang['bank_upload_err_text'] = 'Please Fill all the (*) Marked field correctly';





?>