<?php

/*page texts*/
$lang['page_title_text'] = 'System Settings';
$lang['page_subtitle_text'] = 'Edit and update System settings here';
$lang['box_title_text'] = 'System Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'System Settings';

$lang['mailchimp_separator_lang'] = 'mailchimp';
$lang['thrift_separator_lang'] = 'Thrift Settings';

$lang['label_mailchimp_api_key_text'] = 'Mailchimp api key';
$lang['placeholder_mailchimp_api_key_text'] = 'Enter mailchimp api key';
$lang['label_mailchimp_thrifter_list_name_text'] = 'Thrifter List name';
$lang['placeholder_mailchimp_thrifter_list_name_text'] = 'Enter Thrifter List name';

$lang['label_thrift_warning_1_title_text'] = 'Warning Title';
$lang['tooltip_thrift_warning_1_title_text'] = 'Enter the title for thrift warning' ;
$lang['placeholder_thrift_warning_1_title_text'] = 'Enter the title for thrift warning';

$lang['label_thrift_warning_1_message_text'] = 'Warning Message';
$lang['tooltip_thrift_warning_1_message_text'] = 'Enter the message for thrift warning' ;
$lang['placeholder_thrift_warning_1_message_text'] = 'Enter the message for thrift warning';

$lang['label_thrift_percentage_text'] = 'Thrift Percentage';
$lang['tooltip_thrift_percentage_text'] = 'Only this percentage of an employee salary can be used to join a thrift ' ;
$lang['placeholder_thrift_percentage_text'] = 'Enter The Thrift Perecentage of salary ';

$lang['label_paystack_fees_text'] = 'Paystack fees percentage';
$lang['tooltip_paystack_fees_text'] = 'Enter a number between 1 to 100 ' ;
$lang['placeholder_paystack_fees_text'] = 'Enter percentage of paystack fees percentage  ';

$lang['label_prosperisgold_loan_fees_text'] = 'Loan commision percentage';
$lang['tooltip_prosperisgold_loan_fees_text'] = 'Enter a number between 1 to 100 ' ;
$lang['placeholder_prosperisgold_loan_fees_text'] = 'Enter percentage of Loan commision percentage  ';

$lang['label_stop_new_thrift_text'] = 'Stop New Thrift';
$lang['tooltip_stop_new_thrift_text'] = 'Stop users to create/join new thrifts' ;

$lang['label_allow_inter_org_thrift_text'] = 'Allow Inter Organization Thrift';
$lang['tooltip_allow_inter_org_thrift_text'] = 'Allow members to join a thrift outside of their organization' ;

$lang['label_auto_approve_thrifter_account_text'] = 'Automatically Approve New Thrifter Account';
$lang['tooltip_auto_approve_thrifter_account_text'] = 'If checked, new thrifters will be automatically approved after registration';

$lang['label_custom_thrift_start_delay_text'] = 'Private Thrift Start Delay (days)';
$lang['tooltip_custom_thrift_start_delay_text'] = 'User can  choose the start date after these number of days from today' ;
$lang['placeholder_custom_thrift_start_delay_text'] = 'Enter Private Thrift Start Delay (days) ';

$lang['label_custom_thrift_max_start_time_from_delay_text'] = 'Maximum Private Thrift Start Time (days)';
$lang['tooltip_custom_thrift_max_start_time_from_delay_text'] = 'Number of days from start delay' ;
$lang['placeholder_custom_thrift_max_start_time_from_delay_text'] = 'Enter Maximum Private Thrift Start Time (days) ';

$lang['label_loan_thrift_start_delay_text'] = 'Loan Thrift Start Delay (days)';
$lang['tooltip_loan_thrift_start_delay_text'] = 'User can  choose the start date after these number of days from today' ;
$lang['placeholder_loan_thrift_start_delay_text'] = 'Enter Loan Thrift Start Delay (days) ';

$lang['label_loan_thrift_max_start_time_from_delay_text'] = 'Maximum Loan Thrift Start Time (days)';
$lang['tooltip_loan_thrift_max_start_time_from_delay_text'] = 'Number of days from start delay' ;
$lang['placeholder_loan_thrift_max_start_time_from_delay_text'] = 'Enter Maximum Loan Thrift Start Time (days) ';

$lang['label_individual_thrift_start_delay_text'] = 'Individual Thrift Start Delay (days)';
$lang['tooltip_individual_thrift_start_delay_text'] = 'User can  choose the start date after these number of days from today' ;
$lang['placeholder_individual_thrift_start_delay_text'] = 'Enter Individual Thrift Start Delay (days) ';

$lang['label_individual_thrift_max_start_time_from_delay_text'] = 'Maximum Individual Thrift Start Time (days)';
$lang['tooltip_individual_thrift_max_start_time_from_delay_text'] = 'Number of days from start delay' ;
$lang['placeholder_individual_thrift_max_start_time_from_delay_text'] = 'Enter Maximum Individual Thrift Start Time (days) ';

$lang['label_individual_thrift_minimum_payment_number_text'] = 'Minimum number of payments for individual thrift';
$lang['tooltip_individual_thrift_minimum_payment_number_text'] = 'Minimum number of payments for individual thrift' ;
$lang['placeholder_individual_thrift_minimum_payment_number_text'] = 'Enter minimum number of payments for individual thrift';

$lang['label_individual_thrift_maximum_payment_number_text'] = 'Maximum number of payments for individual thrift';
$lang['tooltip_individual_thrift_maximum_payment_number_text'] = 'Maximum number of payments for individual thrift' ;
$lang['placeholder_individual_thrift_maximum_payment_number_text'] = 'Enter maximum number of payments for individual thrift';

$lang['label_thrift_threshold_time_text'] = 'Thrift Threshold Time';
$lang['tooltip_thrift_threshold_time_text'] = 'The time of the thift starting day , system will start assigning bot to the incomplete group' ;
$lang['placeholder_thrift_threshold_time_text'] = 'Thrift Threshold Time';

$lang['color_separator_lang'] = 'Color';

$lang['label_office_chosen_color_text'] = 'Office Portal Chosen Color';
$lang['label_office_chosen_color_deeper_text'] = 'Office Portal Chosen Color (Deeper)';
$lang['label_partner_chosen_color_text'] = 'Partner Portal Chosen Color';
$lang['label_partner_chosen_color_deeper_text'] = 'Partner Portal Chosen Color (Deeper)';
$lang['label_thrift_chosen_color_text'] = 'Thrift Portal Chosen Color';
$lang['label_thrift_chosen_color_deeper_text'] = 'Thrift Portal Chosen Color (Deeper)';
$lang['label_trustee_chosen_color_text'] = 'Trustee Portal Chosen Color';
$lang['label_trustee_chosen_color_deeper_text'] = 'Trustee Portal Chosen Color (Deeper)';


$lang['button_submit_text'] = 'Update System Settings';

/*Prosperis Gold settings form texts*/


/*validation error texts*/
$lang['thrift_percentage_required_text'] = 'Thrift percentage is required';
$lang['thrift_percentage_numeric_text'] = 'Thrift percentage must be a number';
$lang['thrift_percentage_greater_than_text'] = 'Thrift percentage must be greater than %d';
$lang['thrift_percentage_less_than_text'] = 'Thrift percentage must be  less than %d';

$lang['paystack_fees_required_text'] = 'Paystack fees percentage is required';
$lang['paystack_fees_numeric_text'] = 'Paystack fees percentage must be a number';
$lang['paystack_fees_greater_than_text'] = 'Paystack fees percentage must be greater than %d';
$lang['paystack_fees_less_than_text'] = 'Paystack fees percentage must be  less than %d';

$lang['prosperisgold_loan_fees_required_text'] = 'Loan commision percentage is required';
$lang['prosperisgold_loan_fees_numeric_text'] = 'Loan commision percentage must be a number';
$lang['prosperisgold_loan_fees_greater_than_text'] = 'Loan commision percentage must be greater than %d';
$lang['prosperisgold_loan_fees_less_than_text'] = 'Loan commision percentage must be  less than %d';
/*other texts*/
$lang['noting_to_update_text'] = 'Nothing To Update';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Prosperis Gold Settings';



