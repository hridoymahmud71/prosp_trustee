 <?php

/*page texts*/
 $lang['page_title_text'] = 'Image Settings';
 $lang['page_subtitle_text'] = 'Edit and update image settings here';
 $lang['box_title_text'] = 'Image Settings Form';


 $lang['breadcrumb_home_text'] = 'Home';
 $lang['breadcrumb_section_text'] = 'Settings';
 $lang['breadcrumb_page_text'] = 'Image Settings';

 $lang['image_settings_text'] = 'Original Image Settings';
 $lang['thumb_settings_text'] = 'Thumbnail Settings';


 /*Image settings form texts*/
 $lang['label_encrypt_name_text'] = 'Encrypt Name ?';
 $lang['label_upload_path_text'] = 'Image Upload Path';
 $lang['label_allowed_types_text'] = 'Allowed Image Types';
 $lang['label_max_size_text'] = 'Maximum Image Size (kB)';
 $lang['label_max_width_text'] = 'Maximum Image Size (px)';
 $lang['label_max_height_text'] = 'Maximum Image Size (px)';

 $lang['placeholder_upload_path_text'] = 'Example: project_images';
 $lang['placeholder_allowed_types_text'] = 'Example:  gif | jpg | png | jpeg';
 $lang['placeholder_max_size_text'] = 'Enter Maximum Size (do not enter decimal values)';
 $lang['placeholder_max_width_text'] = 'Enter Maximum Width (do not enter decimal values)';
 $lang['placeholder_max_height_text'] = 'Enter Maximum Height (do not enter decimal values)';

 $lang['option_encrypt_name_yes_text'] = 'Yes, Encrypt Image\'s Name ';
 $lang['option_encrypt_name_no_text'] = 'No, Keep The Original Name';

/*for thumbs */
$lang['label_create_thumb_text'] = 'Want to Create Thumbnail?';
$lang['label_thumb_source_text'] = 'Thumbnail Source';
$lang['label_thumb_marker_text'] = 'Thumb Marker';
$lang['label_maintain_thumb_ratio_text'] = 'Maintain Original Ratio?';
$lang['label_thumb_width_text'] = 'Thumbnail\'s Width';
$lang['label_thumb_height_text'] = 'Thumbnail\'s Height';

$lang['placeholder_thumb_source_text'] = 'Example:  thumb ';
$lang['placeholder_thumb_marker_text'] = 'Enter Thumb\'s suffix .(Example:  _thumb) ';
$lang['placeholder_thumb_width_text'] = 'Enter Thambnail\'s Width (do not enter decimal values)';
$lang['placeholder_thumb_height_text'] = 'Enter Thambnail\'s Height (do not enter decimal values)';

$lang['option_create_thumb_yes_text'] = 'Yes, Create A Thumbnail';
$lang['option_create_thumb_no_text'] = 'No, Don\'t Create Thumbnail';

$lang['option_maintain_thumb_ratio_yes_text'] = 'Yes, Maintain Ratio';
$lang['option_maintain_thumb_ratio_no_text'] = 'No, Don\'t Maintain Ratio';


$lang['button_submit_text'] = 'Update Image Settings';

/*validation error texts*/
 $lang['upload_path_required_text'] = 'Upload Path For Image Is Required';
 $lang['allowed_types_required_text'] = 'Image Types Is Required';

 $lang['max_size_required_text'] = 'Maximum Image File Size Is Required';
 $lang['max_size_integer_text'] = 'Maximum Image File Size Should Be Integer';

 $lang['max_width_required_text'] = 'Maximum Image Width Is Required';
 $lang['max_width_integer_text'] = 'Maximum Image Width Should Be Integer Value';

 $lang['max_height_required_text'] = 'Maximum Image Height Is Required';
 $lang['max_height_integer_text'] = 'Maximum Image Height Should Be Integer Value';

 $lang['thumb_source_required_text'] = 'Thumbnail\'s Source Is Required';

 $lang['thumb_width_required_text'] = 'Thumbnail\'s Width Is Required';
 $lang['thumb_width_integer_text'] = 'Thumbnail\'s Width Should Be Integer Value';

 $lang['thumb_height_required_text'] = 'Thumbnail\'s Height Is Required';
 $lang['thumb_height_integer_text'] = 'Thumbnail\'s Height Should Be Integer Value';



 /*success messages*/
 $lang['update_success_text'] = 'Successfully Updated Image Settings';

