<?php

/*page texts*/
$lang['page_title_text'] = 'Payment Settings';
$lang['page_subtitle_text'] = 'Edit and update Payment settings here';
$lang['box_title_text'] = 'Payment Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Payment Settings';


$lang['label_payment_method_check_text'] = 'Payment Method';
$lang['tooltip_payment_method_check_text'] = 'If payment mehtod is on, user will be required to choose a payment method before various actions';
$lang['paystack_payment_method_check_on_text'] = 'On';
$lang['paystack_payment_method_check_off_text'] = 'Off';

/*paystack <starts>*/
$lang['paystack_separator_lang'] = 'Paystack';

$lang['label_paystack_test_mode_text'] = 'Paystack Test Mode';
$lang['tooltip_paystack_test_mode_text'] = 'Make test mode off before going to live';
$lang['paystack_test_mode_on_text'] = 'On';
$lang['paystack_test_mode_off_text'] = 'Off';


$lang['label_paystack_test_secret_key_text'] = 'Paystack Test Secret Key';
$lang['tooltip_paystack_test_secret_key_text'] = 'This is your paystack test secret key.Do not use the test key in production';
$lang['placeholder_paystack_test_secret_key_text'] = 'Enter Paystack Test Secret Key';

$lang['label_paystack_test_public_key_text'] = 'Paystack Test Public Key';
$lang['tooltip_paystack_test_public_key_text'] = 'This is your paystack test public key.Do not use the test key in production';
$lang['placeholder_paystack_test_public_key_text'] = 'Enter Paystack Test Public Key';

$lang['label_paystack_live_secret_key_text'] = 'Paystack Live Secret Key';
$lang['tooltip_paystack_live_secret_key_text'] = 'This is your paystack live secret key.Use this live secret key in production';
$lang['placeholder_paystack_live_secret_key_text'] = 'Enter Paystack Live Secret Key';

$lang['label_paystack_live_public_key_text'] = 'Paystack Live Public Key';
$lang['tooltip_paystack_live_public_key_text'] = 'This is your paystack live public key.Use this live public key in production';
$lang['placeholder_paystack_live_public_key_text'] = 'Enter Paystack Live Public Key';
/*paystack <ends>*/

/*flutterwave <starts>*/
$lang['flutterwave_separator_lang'] = 'Flutterwave';

$lang['label_flutterwave_environment_text'] = 'Flutterwave Environment';
$lang['tooltip_flutterwave_environment_text'] = 'Make envirionment from \'staging\' to \'production\' before going to live';
$lang['flutterwave_environment_staging_text'] = 'Staging';
$lang['flutterwave_environment_production_text'] = 'Production';


$lang['label_flutterwave_test_merchant_key_text'] = 'Flutterwave Test Merchant Key';
$lang['tooltip_flutterwave_test_merchant_key_text'] = 'This is your flutterwave test merchant key.Do not use this key in production';
$lang['placeholder_flutterwave_test_merchant_key_text'] = 'Flutterwave Test Merchant Key';

$lang['label_flutterwave_test_api_key_text'] = 'Flutterwave Test API Key';
$lang['tooltip_flutterwave_test_api_key_text'] = 'This is your flutterwave live api key.Use this key in production';
$lang['placeholder_flutterwave_test_api_key_text'] = 'Flutterwave Test API Key';

$lang['label_flutterwave_live_merchant_key_text'] = 'Flutterwave Live Merchant Key';
$lang['tooltip_flutterwave_live_merchant_key_text'] = 'This is your flutterwave test merchant key.Use this key in production';
$lang['placeholder_flutterwave_live_merchant_key_text'] = 'Flutterwave Live Merchant Key';

$lang['label_flutterwave_live_api_key_text'] = 'Flutterwave Live API Key';
$lang['tooltip_flutterwave_live_api_key_text'] = 'This is your flutterwave live api key.Use this key in production';
$lang['placeholder_flutterwave_live_api_key_text'] = 'Flutterwave Live API Key';
/*flutterwave <ends>*/

$lang['label_allow_inter_org_thrift_text'] = 'Allow Inter Organization Thrift';
$lang['tooltip_allow_inter_org_thrift_text'] = 'Allow members to join a thrift outside of their organization' ;

$lang['label_custom_thrift_start_delay_text'] = 'Custom Thrift Start Delay (days)';
$lang['tooltip_custom_thrift_start_delay_text'] = 'User can  choose the start date after these number of days from today' ;
$lang['placeholder_custom_thrift_start_delay_text'] = 'Enter Tustom Thrift Start Delay (days) ';


$lang['button_submit_text'] = 'Update Payment Settings';

/*Prosperis Gold settings form texts*/


/*validation error texts*/
$lang['thrift_percentage_required_text'] = 'Thrift percentage is required';
$lang['thrift_percentage_numeric_text'] = 'Thrift percentage must be a number';
$lang['thrift_percentage_greater_than_text'] = 'Thrift percentage must be greater than %d';
$lang['thrift_percentage_less_than_text'] = 'Thrift percentage must be  less than %d';
/*other texts*/
$lang['noting_to_update_text'] = 'Nothing To Update';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Payment Settings';

