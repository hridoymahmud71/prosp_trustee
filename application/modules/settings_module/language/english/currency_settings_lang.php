<?php

/*page texts*/
$lang['page_title_text'] = 'Currency Settings';
$lang['page_subtitle_text'] = 'Edit and update currency settings here';
$lang['box_title_text'] = 'Currency Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Currency Settings';

$lang['add_currency_text'] = 'Add A Currency';
$lang['view_currency_text'] = 'View Currencies';

/*General settings form texts*/
$lang['label_currency_text'] = 'Currency Short Name';
$lang['currency_choose_text'] = 'Choose Default Currency';
$lang['empty_text'] = 'Empty';

$lang['label_currency_name_text'] = 'Currency Name';
$lang['label_conversion_rate_text'] = 'Conversion rate ';
$lang['label_help_usd_to_your_currenct_text'] = '(USD to Your Currency)';
$lang['label_currency_sign_text'] = 'Currency Sign/Symbol ';

$lang['label_currency_position_text_text'] = 'Currency Position';

$lang['option_currency_position_left_along_text'] = 'Alongside Value (left)';
$lang['option_currency_position_right_along_text'] = 'Alongside Value (right)';
$lang['option_currency_position_left_far_text'] = 'Far From Value (left)';
$lang['option_currency_position_right_far_text'] = 'Far From Value (right)';

$lang['button_submit_text'] = 'Update Currency Settings';

/*validation error texts*/
$lang['no_currency_chosen_text'] = 'Please Choose A Currency';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Currency Settings';

/*other*/
$lang['no_currency_found_text'] = 'No Currency is found';
$lang['add_or_activate_currency_text'] = 'Add or activate currency';



