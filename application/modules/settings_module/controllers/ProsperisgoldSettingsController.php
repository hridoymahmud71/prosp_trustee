<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProsperisgoldSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_log_library');

        $this->lang->load('prosperisgold_settings');

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'prosperisgold_settings';
            $data['all_prosperisgold_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            // $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/prosperisgold_settings_page", $data);
            } else {
                $this->load->view("settings_module/prosperisgold_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateProsperisgoldSettings()
    {
        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        $data = array();

        if ($this->input->post('mailchimp_api_key')) {
            $data['mailchimp_api_key'] = trim($this->input->post('mailchimp_api_key'));
        }

        if ($this->input->post('mailchimp_thrifter_list_name')) {
            $data['mailchimp_thrifter_list_name'] = trim($this->input->post('mailchimp_thrifter_list_name'));
        }

        if ($this->input->post('thrift_warning_1_title')) {
            $data['thrift_warning_1_title'] = trim($this->input->post('thrift_warning_1_title'));
        }

        if ($this->input->post('thrift_warning_1_message')) {
            $data['thrift_warning_1_message'] = trim($this->input->post('thrift_warning_1_message'));
        }

        if ($this->input->post('thrift_percentage')) {
            $data['thrift_percentage'] = trim($this->input->post('thrift_percentage'));
        }

        if ($this->input->post('paystack_fees')) {
            $data['paystack_fees'] = trim($this->input->post('paystack_fees'));
        }

        if ($this->input->post('prosperisgold_loan_fees')) {
            $data['prosperisgold_loan_fees'] = trim($this->input->post('prosperisgold_loan_fees'));
        }

        if ($this->input->post('stop_new_thrift')) {
            $data['stop_new_thrift'] = 'stop';
        } else {
            $data['stop_new_thrift'] = 'resume';
        }

        if ($this->input->post('allow_inter_org_thrift')) {
            $data['allow_inter_org_thrift'] = 'allowed';
        } else {
            $data['allow_inter_org_thrift'] = 'not_allowed';
        }

        if ($this->input->post('auto_approve_thrifter_account')) {
            $data['auto_approve_thrifter_account'] = 'on';
        } else {
            $data['auto_approve_thrifter_account'] = 'off';
        }

        if ($this->input->post('custom_thrift_start_delay')) {
            $data['custom_thrift_start_delay'] = trim($this->input->post('custom_thrift_start_delay'));
        }

        if ($this->input->post('custom_thrift_max_start_time_from_delay')) {
            $data['custom_thrift_max_start_time_from_delay'] = trim($this->input->post('custom_thrift_max_start_time_from_delay'));
        }

        if ($this->input->post('loan_thrift_start_delay')) {
            $data['loan_thrift_start_delay'] = trim($this->input->post('loan_thrift_start_delay'));
        }

        if ($this->input->post('loan_thrift_max_start_time_from_delay')) {
            $data['loan_thrift_max_start_time_from_delay'] = trim($this->input->post('loan_thrift_max_start_time_from_delay'));
        }

        if ($this->input->post('individual_thrift_start_delay')) {
            $data['individual_thrift_start_delay'] = trim($this->input->post('individual_thrift_start_delay'));
        }

        if ($this->input->post('individual_thrift_max_start_time_from_delay')) {
            $data['individual_thrift_max_start_time_from_delay'] = trim($this->input->post('individual_thrift_max_start_time_from_delay'));
        }

        if ($this->input->post('individual_thrift_minimum_payment_number')) {
            $data['individual_thrift_minimum_payment_number'] = trim($this->input->post('individual_thrift_minimum_payment_number'));
        }

        if ($this->input->post('individual_thrift_maximum_payment_number')) {
            $data['individual_thrift_maximum_payment_number'] = trim($this->input->post('individual_thrift_maximum_payment_number'));
        }

        if ($this->input->post('thrift_threshold_time')) {
            $data['thrift_threshold_time'] = trim($this->input->post('thrift_threshold_time'));
        }

        /*colors st*/
        if ($this->input->post('office_chosen_color')) {
            $data['office_chosen_color'] = trim($this->input->post('office_chosen_color'));
        }
        if ($this->input->post('office_chosen_color_deeper')) {
            $data['office_chosen_color_deeper'] = trim($this->input->post('office_chosen_color_deeper'));
        }

        if ($this->input->post('partner_chosen_color')) {
            $data['partner_chosen_color'] = trim($this->input->post('partner_chosen_color'));
        }
        if ($this->input->post('partner_chosen_color_deeper')) {
            $data['partner_chosen_color_deeper'] = trim($this->input->post('partner_chosen_color_deeper'));
        }

        if ($this->input->post('thrift_chosen_color')) {
            $data['thrift_chosen_color'] = trim($this->input->post('thrift_chosen_color'));
        }
        if ($this->input->post('thrift_chosen_color_deeper')) {
            $data['thrift_chosen_color_deeper'] = trim($this->input->post('thrift_chosen_color_deeper'));
        }

        if ($this->input->post('trustee_chosen_color')) {
            $data['trustee_chosen_color'] = trim($this->input->post('trustee_chosen_color'));
        }
        if ($this->input->post('trustee_chosen_color_deeper')) {
            $data['trustee_chosen_color_deeper'] = trim($this->input->post('trustee_chosen_color_deeper'));
        }
        /*colors en*/


        if ($data == null) {
            $this->session->set_flashdata('noting_to_update', $this->lang->line('noting_to_update_text'));
        }

        /*set rules starts*/
        if ($this->input->post('thrift_percentage')) {

            $this->form_validation->set_rules('thrift_percentage', 'Thrift percentage', 'required|numeric|greater_than[0]|less_than[100]',
                array(
                    'required' => $this->lang->line('thrift_percentage_required_text'),
                    'numeric' => $this->lang->line('thrift_percentage_numeric_text')

                )
            );
        }

        /*set rules starts*/
        if ($this->input->post('paystack_fees')) {

            $this->form_validation->set_rules('paystack_fees', 'Paystack fees', 'required|numeric|greater_than[0]|less_than[100]',
                array(
                    'required' => $this->lang->line('paystack_fees_required_text'),
                    'numeric' => $this->lang->line('paystack_fees_numeric_text')

                )
            );
        }

        /*set rules starts*/
        if ($this->input->post('prosperisgold_loan_fees')) {

            $this->form_validation->set_rules('prosperisgold_loan_fees', 'Prosperisgold loan fees', 'required|numeric|greater_than[0]|less_than[100]',
                array(
                    'required' => $this->lang->line('prosperisgold_loan_fees_required_text'),
                    'numeric' => $this->lang->line('prosperisgold_loan_fees_numeric_text')

                )
            );
        }

        if ($this->input->post('custom_thrift_start_delay')) {

            $this->form_validation->set_rules('custom_thrift_start_delay', 'Private Thrift Start Delay', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[15]');
        }

        if ($this->input->post('custom_thrift_max_start_time_from_delay')) {

            $this->form_validation->set_rules('custom_thrift_max_start_time_from_delay', 'Maximum Private Thrift Start Time', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[60]');
        }

        if ($this->input->post('individual_thrift_start_delay')) {

            $this->form_validation->set_rules('individual_thrift_start_delay', 'Individual Thrift Start Delay', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[15]');
        }

        if ($this->input->post('individual_thrift_max_start_time_from_delay')) {

            $this->form_validation->set_rules('individual_thrift_max_start_time_from_delay', 'Individual Private Thrift Start Time', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[90]');
        }

        if ($this->input->post('individual_thrift_minimum_payment_number')) {

            $this->form_validation->set_rules('individual_thrift_minimum_payment_number', 'Minimum number of payments for individual thrift', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[24]');
        }

        if ($this->input->post('individual_thrift_maximum_payment_number')) {

            $this->form_validation->set_rules('individual_thrift_maximum_payment_number', 'Maximum number of payments for individual thrift', 'required|numeric|greater_than_equal_to[1]|less_than_equal_to[48]');
        }


        /*set rules ends*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
        } else {

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'prosperisgold_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'system_settings',                                                      //3.    $type
                '',                                                                     //4.    $type_id
                'updated',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('settings_module/prosperisgold_settings');

    }


}