
        <div class="container">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?= lang('reports_by_time_text') ?>
                            -
                            <?php
                            if ($payment_or_recieve == 'payment_report') {
                                echo lang('payment_report_text');
                            }


                            if ($payment_or_recieve == 'payment_recieve_report') {
                                echo lang('payment_recieve_report_text');
                            }
                            ?>
                        </h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#"><?= lang('breadcrum_report_text') ?></a></li>
                            <li class="breadcrumb-item active"><?= lang('breadcrum_report_details_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">
                            <b>
                                <?php
                                if ($years_or_months == 'years') {
                                    echo lang('years_text');
                                }

                                if ($years_or_months == 'months') {
                                    echo lang('months_text');
                                }

                                echo ' - ';

                                if ($time_range == 'all') {
                                    echo lang('all_text');
                                }

                                if ($time_range == 'all_year') {
                                    echo lang('all_years_text');
                                }

                                if ($years_or_months == 'current_year') {
                                    echo lang('current_year_text');
                                }
                                ?>
                            </b>
                        </h4>

                        <table id="year-month-table" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>
                                    <?php
                                    if ($years_or_months == 'years') {
                                        echo lang('years_text');
                                    }

                                    if ($years_or_months == 'months') {
                                        echo lang('months_text');
                                    }
                                    ?>
                                </th>
                                <th><?php echo lang('report_text') ?></th>
                            </tr>
                            </thead>


                            <tbody>

                            <?php if ($year_list) { ?>
                                <?php foreach ($year_list as $year) { ?>
                                    <tr>
                                        <td><?php echo $year['year'] ?></td>
                                        <td>
                                            <a title="" href="<?php echo $year['payment_report_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                            &nbsp;&nbsp;
                                            <a title="" href="<?php echo $year['payment_report_pdf_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></a>
                                            &nbsp;&nbsp;
                                            <a title="" href="<?php echo $year['payment_report_excel_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-file-excel-o fa-lg" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>

                            <?php if ($month_list) { ?>
                                <?php foreach ($month_list as $month) { ?>
                                    <tr>
                                        <td data-sort="<?php echo $month['month_sort'] ?>" ><?php echo $month['month'] ?></td>
                                        <td >
                                            <a title="" href="<?php echo $month['payment_report_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                            &nbsp;&nbsp;
                                            <a title="" href="<?php echo $month['payment_report_pdf_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></a>
                                            &nbsp;&nbsp;
                                            <a title="" href="<?php echo $month['payment_report_excel_anchor'] ?>" style="color:#2b2b2b">
                                                <i class="fa fa-file-excel-o fa-lg" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end row -->


        </div> <!-- container -->




<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<script>
    $(function () {

        $('#year-month-table').DataTable();

    });
</script>