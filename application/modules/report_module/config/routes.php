<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//report months - year list only

//payment
$route['report_module/all_report/payment_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/all_report/payment_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/all_report/payment_report/months/current_year'] = 'ReportController/showReportYearMonths';

//any = employer_id
$route['report_module/employer_report/payment_report/years/all/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report/payment_report/months/all_year/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report/payment_report/months/current_year/(:any)'] = 'ReportController/showReportYearMonths';

//any = employee_id
$route['report_module/employee_report/payment_report/years/all/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report/payment_report/months/all_year/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report/payment_report/months/current_year/(:any)'] = 'ReportController/showReportYearMonths';

/*---------------------------------*/
$route['report_module/employer_report_as_employer/payment_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report_as_employer/payment_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report_as_employer/payment_report/months/current_year'] = 'ReportController/showReportYearMonths';

$route['report_module/employee_report_as_employee/payment_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report_as_employee/payment_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report_as_employee/payment_report/months/current_year'] = 'ReportController/showReportYearMonths';

/*---------------------------------------------------------------------------------------------------------------------*/

//recieve
$route['report_module/all_report/payment_recieve_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/all_report/payment_recieve_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/all_report/payment_recieve_report/months/current_year'] = 'ReportController/showReportYearMonths';

//any = employer_id
$route['report_module/employer_report/payment_recieve_report/years/all/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report/payment_recieve_report/months/all_year/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report/payment_recieve_report/months/current_year/(:any)'] = 'ReportController/showReportYearMonths';

//any = employee_id
$route['report_module/employee_report/payment_recieve_report/years/all/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report/payment_recieve_report/months/all_year/(:any)'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report/payment_recieve_report/months/current_year/(:any)'] = 'ReportController/showReportYearMonths';

/*---------------------*/
$route['report_module/employer_report_as_employer/payment_recieve_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report_as_employer/payment_recieve_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/employer_report_as_employer/payment_recieve_report/months/current_year'] = 'ReportController/showReportYearMonths';

$route['report_module/employee_report_as_employee/payment_recieve_report/years/all'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report_as_employee/payment_recieve_report/months/all_year'] = 'ReportController/showReportYearMonths';
$route['report_module/employee_report_as_employee/payment_recieve_report/months/current_year'] = 'ReportController/showReportYearMonths';



/*--------------------------------------------------------------------------------------------------------------------*/

$route['report_module/all_employer'] = 'ReportController/allEmployer';
$route['report_module/get_all_employer_by_ajax'] = 'ReportController/getAllEmployerByAjax';

$route['report_module/employee/all'] = 'ReportController/employee';
$route['report_module/employee/my'] = 'ReportController/employee';

$route['report_module/employee_report_pdf/all'] = 'ReportController/employee';
$route['report_module/employee_report_pdf/my'] = 'ReportController/employee';

$route['report_module/employee_report_excel/all'] = 'ReportController/employee';
$route['report_module/employee_report_excel/my'] = 'ReportController/employee';

$route['report_module/get_employee_by_ajax'] = 'ReportController/getEmployeeByAjax';

/*------------------------------------------------------------*/



//reports: datatable,pdf and excel
/*--------------------------------------------------------------------------------------------------------------------*/

$route['report_module/show_report/all_report/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_grouped_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_grouped_report/all'] = 'ReportController/showReport';

$route['report_module/show_report/all_report/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_recieve_grouped_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_recieve_grouped_report/all'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/all_report/payment_with_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_with_recieve_report/all'] = 'ReportController/showReport';

//any = year or year-month
$route['report_module/show_report/all_report/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_grouped_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_grouped_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/all_report/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/all_report/payment_recieve_grouped_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_recieve_grouped_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/all_report/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/all_report/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';


/*------------------------------------------------------------*/



/*------------------------------------------------------------*/

//any = employer_id
$route['report_module/show_report/employer_report/payment_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report/payment_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_report/all/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employer_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employer_report/payment_with_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_with_recieve_report/all/(:any)'] = 'ReportController/showReport';

//any_1st = year or year-month, any_2nd = employer_id
$route['report_module/show_report/employer_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employer_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employer_report/payment_with_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report/payment_with_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';

/*------------------------------------------------------------*/



/*------------------------------------------------------------*/

//any = employee_id
$route['report_module/show_report/employee_report/payment_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report/payment_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_report/all/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employee_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_recieve_report/all/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employee_report/payment_with_recieve_report/all/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_with_recieve_report/all/(:any)'] = 'ReportController/showReport';


//any_1st = year or year-month, any_2nd = employee_id
$route['report_module/show_report/employee_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_report/(:any)/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employee_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employee_report/payment_with_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report/payment_with_recieve_report/(:any)/(:any)'] = 'ReportController/showReport';

/*------------------------------------------------------------*/



/*------------------------------------------------------------*/

$route['report_module/show_report/employer_report_as_employer/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report_as_employer/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_report/all'] = 'ReportController/showReport';

$route['report_module/show_report/employer_report_as_employer/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report_as_employer/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_recieve_report/all'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employer_report_as_employer/payment_with_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_with_recieve_report/all'] = 'ReportController/showReport';


//any = year or year-month
$route['report_module/show_report/employer_report_as_employer/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report_as_employer/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employer_report_as_employer/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employer_report_as_employer/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_recieve_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employer_report_as_employer/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employer_report_as_employer/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';

/*------------------------------------------------------------*/



/*------------------------------------------------------------*/

$route['report_module/show_report/employee_report_as_employee/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report_as_employee/payment_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_report/all'] = 'ReportController/showReport';

$route['report_module/show_report/employee_report_as_employee/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report_as_employee/payment_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_recieve_report/all'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employee_report_as_employee/payment_with_recieve_report/all'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_with_recieve_report/all'] = 'ReportController/showReport';


//any = year or year-month
$route['report_module/show_report/employee_report_as_employee/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report_as_employee/payment_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report/employee_report_as_employee/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_pdf/employee_report_as_employee/payment_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_recieve_report/(:any)'] = 'ReportController/showReport';

$route['report_module/show_report_pdf/employee_report_as_employee/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';
$route['report_module/show_report_excel/employee_report_as_employee/payment_with_recieve_report/(:any)'] = 'ReportController/showReport';

/*------------------------------------------------------------*/

/*ajax*/
$route['report_module/get_payment_report_by_ajax'] = 'ReportController/getPaymentReportByAjax';
$route['report_module/get_payment_recieve_report_by_ajax'] = 'ReportController/getPaymentRecieveReportByAjax';





  
