<?php


$lang['page_title_all_report_details_page_text'] = 'All Report Details page';
$lang['page_title_employer_report_details_page_text'] = 'Organization\'s Report Details page';
$lang['page_title_employee_report_details_page_text'] = 'Thrifter\'s Report Details page';

$lang['breadcrum_report_text'] = 'Report';
$lang['breadcrum_report_details_text'] = 'Report Details';


/*payments col*/

$lang['thrift_group_number_text'] = 'Thrift Group ID';
$lang['thrift_group_employer_text'] = 'Organization';
$lang['thrift_group_payment_recieve_amount_text'] = 'Amount';
$lang['thrift_group_member_text'] = 'Member';
$lang['thrift_group_payment_recieve_number_text'] = 'Payment Receive ID';
$lang['thrift_group_payment_date_text'] = 'Disbursement Date';
$lang['thrift_group_start_date_text'] = 'Thrift Start';
$lang['thrift_group_end_date_text'] = 'Thrift End';

$lang['thrift_group_thrifter_id_text'] = 'Thrifter ID';
$lang['thrift_group_payee_member_text'] = 'Payee';

$lang['total_text'] = 'Total';


$lang['thrift_details_text'] = 'Thrift Details';
$lang['member_details_text'] = 'Member Details';
$lang['payment_details_text'] = 'Payment Details';
$lang['payment_recieve_details_text'] = 'Payment Disbursement';

$lang['yes_text'] = 'Yes';
$lang['no_text'] = 'No';

$lang['unavailable_text'] = 'Unavailable';
$lang['choose_range_text'] = 'Choose Range';

$lang['payment_recieve_pdf_text'] = 'DISBURSEMENT PDF';
$lang['payment_with_disbursement_pdf_text'] = 'PAYMENT/DISBURSMENT PDF';

$lang['payment_recieve_excel_text'] = 'DISBURSEMENT EXCEL';
$lang['payment_with_disbursement_excel_text'] = 'PAYMENT/DISBURSMENT EXCEL';

$lang['other_reports_text'] = 'OTHER REPORTS';

$lang['payment_recieve_grouped_pdf_text'] = 'DISBURSEMENT BY GROUP PDF';
$lang['payment_recieve_grouped_excel_text'] = 'DISBURSEMENT BY GROUP EXCEL';

/**/
$lang['loading_text'] = 'Loding...';
$lang['not_found_text'] = 'Not found';
$lang['no_matching_found_text'] = 'No Matching Result Found';




