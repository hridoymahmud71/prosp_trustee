<?php

/*page texts*/
$lang['page_title_text'] = 'Contact';
$lang['page_subtitle_text'] = 'Contact Information';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Contact View';


/*admin*/
$lang['admin_box_title_text'] = 'System Admin';

$lang['system_admin_contact_email_text'] = 'Email';
$lang['system_admin_contact_phone_text'] = 'Phone';

/*company*/
$lang['company_box_title_text'] = 'Company';

$lang['company_contact_email_text'] = 'Email';
$lang['company_contact_phone_text'] = 'Phone';
$lang['company_contact_address_text'] = 'Address';

$lang['company_facebook_id_text'] = 'Facebook Id';
$lang['company_twitter_id_text'] = 'Twitter Id';
$lang['company_youtube_text'] = 'Youtube Id';

/*other*/
$lang['unavailable_text'] = 'Unavailable';

?>