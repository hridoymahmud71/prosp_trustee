<?php

/*page texts*/
$lang['page_title_text'] = 'Currencies';
$lang['page_subtitle_text'] = 'Add | Show | Edit | Delete  Currencies';
$lang['table_title_text'] = 'Currency List';
$lang['no_currency_found_text'] = 'No Currency Is Found !';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Currency Settings';
$lang['breadcrumb_page_text'] = 'Currency List';

$lang['add_button_text'] = 'Add A Currency';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';
$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';


$lang['column_currency_name_text'] = 'Currency Name';
$lang['column_currency_short_name_text'] = 'Short Name';
$lang['column_currency_sign_text'] = 'Sign/Symbol';
$lang['column_currency_conversion_rate_text'] = 'Conversion Rate';
$lang['column_currency_status_text'] = 'Status';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

$lang['column_actions_text'] = 'Actions';

/*other texts*/
$lang['confirm_delete_text'] = 'Are You Sure To Delete This Currency ? ';
$lang['delete_success_text'] = 'Succesfully deleted the currency.';

/*success messages*/
$lang['successfull_text'] = 'Succesfull';
$lang['add_successfull_text'] = 'Succesfully added the currency';
$lang['update_successfull_text'] = 'Succesfully updated the currency.';

$lang['currency_activated'] = ' is activated';
$lang['currency_deactivated'] = ' is deactivated';
$lang['currency_deleted'] = ' is deleted';


/*sweetalert lang not working*/
$lang['swal_title'] = 'Are you sure to delete this Currency?';
$lang['swal_confirm_button_text'] = 'Yes, delete it!';
$lang['swal_cancel_button_text'] = 'No, cancel please!';



/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Currency Active';
$lang['tooltip_deactivate_text'] = 'Make Currency Deactive';

$lang['tooltip_edit_text'] = 'Edit Currency ';
$lang['tooltip_delete_text'] = 'Delete Currency ';






