<?php

$lang['page_title_text'] = 'Edit Organization';

$lang['breadcrum_home_text'] = 'Organization Management';
$lang['breadcrumb_page_add_text'] = 'Edit Organization';

$lang['employee_add_form_header_text'] = "Update the Organization Information";

/*page texts*/
$lang['employer_company_text'] = 'Organization name';

$lang['employer_first_name_text'] = 'First Name';

$lang['employer_last_name_text'] = 'Last Name';

$lang['employer_home_address_text'] = 'Address';

$lang['employer_phone_text'] = 'Phone';

$lang['employer_contact_person_text'] = 'Contact Person';

$lang['employer_email_text'] = 'Email';

$lang['employer_website_text'] = 'Website';

$lang['employer_subdomain_text'] = 'Subdomain';

$lang['employer_industry_text'] = 'Industry';

$lang['file_submit_text'] = 'SUBMIT';
$lang['modal_cancel_text'] = 'RESET';


// flash message

$lang['employer_success_update_text'] = 'Organization Details is Successfully Updated';
$lang['employer_update_error_text'] = 'All (*) marked Field is required';
$lang['subdomain_error_text'] = 'Subdomain already exists';


?>