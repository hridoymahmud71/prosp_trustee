<?php

/*page texts*/
$lang['page_title_text'] = 'Edit Personal Information';
$lang['page_subtitle_text'] = 'Edit personal information here';
$lang['box_title_text'] = 'Information Form';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'User Profile';
$lang['breadcrumb_page_text'] = 'Edit personal info';

$lang['address_separator_lang'] = 'Address';
$lang['bank_info_separator_lang'] = 'Bank Information';

/*General settings form texts*/
$lang['label_first_name_text'] = 'First Name';
$lang['label_last_name_text'] = 'Last Name';
$lang['label_company_text'] = 'Company';
$lang['label_email_text'] = 'Default Email';
$lang['label_user_additional_email_text'] = 'Additional Email';
$lang['label_subdomain_text'] = 'Subdomain';

$lang['label_user_salary_text'] = 'Salary';

$lang['label_change_password_text'] = ' Change Password ?';
$lang['option_change_password_yes_text'] = 'Yes, change my password';
$lang['option_change_password_no_text'] = 'No, keep as it is';

$lang['label_password_text'] = 'Password';
$lang['label_confirm_password_text'] = 'Confirm Password';

$lang['label_user_profile_image_text'] = 'Profile Image';
$lang['img_title_user_profile_image_text'] = 'Preview Profile Picture';

$lang['label_user_cover_image_text'] = 'Cover Image';
$lang['img_title_user_cover_image_text'] = 'Preview Profile Picture';

$lang['label_phone_text'] = 'Default Phone Number';
$lang['label_user_additional_phone_text'] = 'Additional Phone Number';

$lang['label_user_show_age_text'] = 'Show Age?';
$lang['option_show_age_yes_text'] = 'Yes, show my age';
$lang['option_show_age_no_text'] = 'No, do not show my age';

$lang['label_user_age_text'] = 'Age';
$lang['label_user_dob_text'] = 'Date of Birth';
$lang['label_user_position_text'] = 'Position in Company';
$lang['label_user_home_address_text'] = 'Home Address';
$lang['label_user_office_address_text'] = 'Office Address';

$lang['label_user_street_1_text'] = 'Street 1';
$lang['label_user_street_2_text'] = 'Street 2';
$lang['label_user_country_text'] = 'Country';
$lang['label_user_state_text'] = 'State';
$lang['label_user_city_text'] = 'City';

/*--------------------------------------------------------*/
//bank_select_text
//label_user_bank_account_no_text
//placeholder_user_bank_account_no_text

$lang['bank_select_text'] = 'Select Bank';
$lang['label_user_bank_account_no_text'] = 'Bank Account Number';
$lang['placeholder_user_bank_account_no_text'] = 'Bank Account Number';
$lang['placeholder_user_bank_account_no_text'] = 'Enter Bank Account';
/*-------------------------------------------------------*/

$lang['placeholder_first_name_text'] = 'Enter First Name';
$lang['placeholder_last_name_text'] = 'Enter Last Name';
$lang['placeholder_company_text'] = 'Enter Company';
$lang['placeholder_email_text'] = 'Default Email';
$lang['placeholder_user_additional_email_text'] = 'Enter Additional Email';
$lang['placeholder_subdomain_text'] = 'Enter subdomain';

$lang['placeholder_password_text'] = 'New Password';
$lang['placeholder_confirm_password_text'] = 'Confirm New Password';

$lang['upload_user_profile_image_text'] = 'Upload Profile Image';
$lang['help_user_profile_image_text'] = 'Choose A square shaped photo for better view';

$lang['upload_user_cover_image_text'] = 'Upload Cover Image';
$lang['help_user_cover_image_text'] = 'Choose A reactangular shaped photo for better view';

$lang['placeholder_phone_text'] = 'Enter Main Phone Number (default)';
$lang['placeholder_user_additional_phone_text'] = 'Enter An additional phone number';

$lang['placeholder_user_age_text'] = 'Enter Age';
$lang['placeholder_user_dob_text'] = 'Enter Date of Birth';
$lang['placeholder_user_salary_text'] = 'Enter Salary';
$lang['placeholder_user_position_text'] = 'Enter Position in the Company';
$lang['placeholder_user_home_address_text'] = 'Enter Home address';
$lang['placeholder_user_office_address_text'] = 'Enter Office Address';

$lang['placeholder_user_street_1_text'] = 'Enter Street 1';
$lang['placeholder_user_street_2_text'] = 'Enter Street 2';
$lang['placeholder_user_country_text'] = 'Select a Country';
$lang['placeholder_user_state_text'] = 'Select a State';
$lang['placeholder_user_city_text'] = 'Select a City';

$lang['button_submit_text'] = 'Update Information';

/*validation error texts*/
$lang['first_name_required_text'] = 'First Name is Required';
$lang['last_name_required_text'] = 'Last Name is Required';
$lang['company_required_text'] = 'Company is Required';

$lang['email_required_text'] = 'Default Email is Required';
$lang['email_valid_email_text'] = 'Default email is invalid';
$lang['user_additional_email_valid_email_text'] = 'Additional Email is not valid';
$lang['user_salary_numeric_text'] = 'Salary must be a valid amount';

$lang['password_required_text'] = 'Password is Required';
$lang['password_min_length_text'] = 'Minimum Password Length is %1$s characters';
$lang['password_max_length_text'] = 'Maximum Password Length is %1$s characters';

$lang['confirm_password_required_text'] = 'Password Confirmation is Required';
$lang['password_and_confirm_password_not_match_text'] = 'Password and Password Confirmation doesn\'t match';

$lang['user_bank_required_text'] = 'You have to select a bank';
$lang['user_bank_account_no_required_text'] = 'Bank account number is needed';

$lang['user_show_age_required_text'] = 'Choose whether to show age or not';
$lang['user_age_required_text'] = 'Age is Reqiired';
$lang['user_age_integer_text'] = 'Age must be An Integer Number';
$lang['user_age_greater_than_equal_to_text'] = 'Age must be atleast 1';

$lang['for_profile_image_text'] = 'For Profile Image:';
$lang['for_cover_image_text'] = 'For Cover Image:';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Information';

$lang['not_found_text'] = 'Not Found';

$lang['country_not_found_text'] = 'Country Not Found';
$lang['state_not_found_text'] = 'State Not Found';
$lang['city_not_found_text'] = 'City Not Found';




