<?php

/*page_texts*/
$lang['page_title_text'] = 'Timeline';
$lang['page_subtitle_text'] = 'Activities in reverse chronological order';
$lang['box_title_text'] = 'Timeline';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'User Profile';
$lang['breadcrumb_page_text'] = 'Timeline';


$lang['navtab_timeline_title_text'] = 'Timeline';

$lang['navtab_my_activity_title_text'] = 'My Activity';
$lang['navtab_user_activity_title_text'] = 'User\'s Activity';

$lang['navtab_related_activity_title_text'] = 'Related Activity';


/*other texts*/
$lang['go_text'] = 'Go';
$lang['remove_text'] = 'Remove';

$lang['today_text'] = 'Today';
$lang['yesterday_text'] = 'Yesterday';

$lang['timeline_empty_text'] = 'Nothing To Show on Timeline';
$lang['user_timeline_empty_text'] = 'No Activity by User';
$lang['my_timeline_empty_text'] = 'No Activity by Me';

//user_timeline_empty_text

$lang['related_timeline_empty_text'] = 'Nothing To Show on Related Timeline';


/*log text*/
$lang['admin_text'] = 'Admin';
$lang['admin_text'] = 'Admin';
$lang['log_created_for_string_text'] = 'This timeline item is created for %1$s';
$lang['log_created_for_you_text'] = 'This timeline item is created for You';

?>