<?php

$lang['upms_info_text'] = 'Info';
$lang['upms_timeline_text'] = 'Timeline';

$lang['upms_my_projects_text'] = 'My Projects';
$lang['upms_users_projects_text'] = 'User\'s Projects';

$lang['upms_running_text'] = 'Running ';

$lang['upms_my_tasks_text'] = 'My Tasks';
$lang['upms_users_tasks_text'] = 'User\'s Tasks';

$lang['upms_no_position_text'] = 'No Position';
$lang['upms_no_company_text'] = 'No Company';
?>