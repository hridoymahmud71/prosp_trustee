<!--<div class="content-page">-->


<?php if ($this->session->flashdata('thrift_error')) { ?>
    <section class="content mt-0">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="alert-heading"><?php echo lang('unsuccessful_text') ?></h4>

                    <?php if ($this->session->flashdata('thrift_error_exceed_limit_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_exceed_limit_text') ?>
                        </p>
                    <?php } ?>

                    <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                        </p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
<?php } ?>

<!-- Start content -->
<!--    <div class="content">-->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php if ($is_admin == 'admin') echo lang('page_subtitle_admin_dashboard_text') ?>
                    <?php if ($is_trustee == 'trustee') echo lang('page_subtitle_trustee_dashboard_text') ?>
                    <?php if ($is_employer == 'employer') echo lang('page_subtitle_employer_dashboard_text') ?>
                    <?php if ($is_employee == 'employee') echo lang('page_subtitle_employee_dashboard_text') ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->


    <?php if ($is_trustee == 'trustee') { ?>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-user float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_last_month_payment_text') ?></h6>
                    <h4 class="m-b-20">
                        <span>
                            <?= $last_month_payment[0]['total_payment_count']; ?>/<?= $currency; ?><?= number_format($last_month_payment[0]['total_payment_amount']); ?>
                        </span>

                    </h4>
                    <!-- <span class="label label-success"> +11% </span> <span class="text-muted">From previous period</span> -->
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class=" icon-emotsmile float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_this_month_payment_text') ?></h6>
                    <h4 class="m-b-20">
                        <span>
                            <?= $this_month_payment[0]['total_payment_count']; ?>/<?= $currency; ?><?= number_format($this_month_payment[0]['total_payment_amount']); ?>
                        </span>

                    </h4>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-user-following float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_next_month_payment_text') ?></h6>
                    <h4 class="m-b-20">
                        <span><?= $next_month_payment[0]['total_payment_count']; ?>/<?= $currency; ?><?= number_format($next_month_payment[0]['total_payment_amount']); ?></span>
                    </h4>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-people float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_total_paid_text') ?></h6>
                    <h4 class="m-b-20">
                        <span><?= $total_paid[0]['total_payment_count']; ?>/<?= $currency; ?><?= number_format($total_paid[0]['total_payment_amount']); ?></span>
                    </h4>
                </div>
            </div>

        </div>

    <?php } ?>

    <?php if ($is_trustee == 'trustee') { ?>
        <hr>
    <?php } ?>
    <div class="row" <?php if ($is_trustee != 'trustee') { ?> style="display: none" <?php } ?> >
        <div class="col-xs-12 col-md-9">
            <div class="card-box">
                <div class="p-20 p-b-0">
                    <div class="text-center">
                        <ul class="list-inline chart-detail-list">
                            <li class="list-inline-item">
                                <h6 style="color: #3db9dc;"><i
                                            class="zmdi zmdi-circle-o m-r-5"></i><?php echo lang('collections_text') ?>
                                </h6>
                            </li>
                            <li class="list-inline-item">
                                <h6 style="color: #ff5d48;"><i
                                            class="zmdi zmdi-square-o m-r-5"></i><?php echo lang('disbursments_text') ?>
                                </h6>
                            </li>
                        </ul>
                    </div>
                    <div id="monthly-collections-disbursements" style="height: 320px;"></div>

                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-20"><?= lang('inbox_text') ?> (<?= $count_messages ?>)</h4>
                <a href="message_module/inbox" type="button"
                   class="btn btn-info btn-rounded waves-effect waves-light"><?= lang('view_all_text') ?></a>

                <?php if ($messages_and_comments) { ?>
                <div class="inbox-widget nicescroll"
                     style="height: 250px;
                                 tabindex=" 5000>

                    <?php foreach ($messages_and_comments as $messages_and_comment) { ?>
                        <a href="<?= $messages_and_comment->url ?>">
                            <div class="inbox-item">
                                <!--<div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg"
                                                                 class="rounded-circle" alt=""></div>-->
                                <p class="inbox-item-author"><?= $messages_and_comment->sender ?></p>
                                <p class="inbox-item-text"><?= $messages_and_comment->text ?></p>
                                <p class="inbox-item-date"><?= $messages_and_comment->datestring ?></p>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <?php } ?>

                <?php if (!$messages_and_comments || empty($messages_and_comments)) { ?>
                    <p><?= lang('no_unread_messages_text') ?></p>
                <?php } ?>

            </div>

        </div><!-- end col-->

    </div>

</div>


<!-- Chart JS -->
<script src="assets/backend_assets/plugins/chart.js/chart.min.js"></script>

<!--Morris Chart-->
<script src="assets/backend_assets/plugins/morris/morris.min.js"></script>
<script src="assets/backend_assets/plugins/raphael/raphael-min.js"></script>


<script type="text/javascript">

    Morris.Bar({
        element: 'monthly-collections-disbursements',
        barSizeRatio: 0.3,
        gridTextSize: '10px',
        hideHover: "true",
        data: <?=$month_wise_data?>,
        xkey: 'y',
        ykeys: ['collection', 'disbursment'],
        labels: ['Collections', 'Disbursments'],
        barColors: ['#3db9dc', '#ff5d48']
    });


</script>