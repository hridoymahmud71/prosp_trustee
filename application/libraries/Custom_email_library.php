<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */


class Custom_email_library
{
    public $CI;

    public $if_settings_type_exists;
    public $all_email_settings = array();


    public function __construct()
    {
        $this->CI = &get_instance();


        $this->CI->load->library('session');
        $this->CI->load->library('custom_datetime_library');
        $this->CI->load->library('upload');

        //customized lib from modules/settings_module/libraries
        $this->CI->load->library('settings_module/custom_settings_library');

        $a_settings_code = 'email_settings';
        $this->if_settings_type_exists = $this->CI->custom_settings_library->ifSettingsTypeExist($a_settings_code);

        if ($this->if_settings_type_exists == true) {
            $this->all_email_settings = $this->CI->custom_settings_library->getSettings($a_settings_code);
        } else {
            $this->all_email_settings = null;
        }


    }

    public function getProtocol()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'protocol') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return false;

                    }

                }

            }

        } else {
            return false;
        }
    }

    public function get_SMTP_HOST()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'smtp_host') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return '';

                    }

                }

            }

        } else {
            return '';
        }
    }

    public function get_SMTP_PORT()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'smtp_port') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return '';

                    }

                }

            }

        } else {
            return '';
        }
    }

    public function get_SMTP_TIMEOUT()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'smtp_timeout') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return '';

                    }

                }

            }

        } else {
            return '';
        }
    }

    public function get_SMTP_USER()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'smtp_user') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return '';

                    }

                }

            }

        } else {
            return '';
        }
    }

    public function get_SMTP_PASS()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'smtp_pass') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return '';

                    }

                }

            }

        } else {
            return '';
        }
    }

    public function getMailType()
    {
        if ($this->all_email_settings) {

            foreach ($this->all_email_settings as $an_email_settings) {

                if ($an_email_settings->settings_key == 'mailtype') {

                    if ($an_email_settings->settings_value != '') {

                        return $an_email_settings->settings_value;

                    } else {
                        return 'text';

                    }

                }

            }

        } else {
            return 'text';
        }
    }



    /*unused function */
    /*http://nazmulahsan.me/simple-two-way-function-encrypt-decrypt-string/*
   /*public function my_simple_crypt( $string, $action = 'e' ) {
       // you may change these values to your own
       $secret_key = 'my_simple_secret_key';
       $secret_iv = 'my_simple_secret_iv';

       $output = false;
       $encrypt_method = "AES-256-CBC";
       $key = hash( 'sha256', $secret_key );
       $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

       if( $action == 'e' ) {
           $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
       }
       else if( $action == 'd' ){
           $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
       }

       return $output;
   }*/

    public function getEmailConfig($mailtype_html_or_text = false)
    {

        if ($this->getProtocol() == false) {
            return false;
        } else {
            $config['protocol'] = $this->getProtocol();
        }

        if ($config['protocol'] == 'smtp') {

            if ($this->get_SMTP_HOST() != '') {
                $config['smtp_host'] = $this->get_SMTP_HOST();
            } else {
                return false;
            }

            if ($this->get_SMTP_PORT() != '') {
                $config['smtp_port'] = $this->get_SMTP_PORT();
            } else {
                return false;
            }

            if ($this->get_SMTP_TIMEOUT() != '') {
                $config['smtp_timeout'] = $this->get_SMTP_TIMEOUT();
            } else {
                return false;
            }

            if ($this->get_SMTP_USER() != '') {
                $config['smtp_user'] = $this->get_SMTP_USER();
            } else {
                return false;
            }

            if ($this->get_SMTP_PASS() != '') {
                $config['smtp_pass'] = $this->get_SMTP_PASS();
            } else {
                return false;
            }

        }

        $config['wordwrap'] = TRUE;
        $config['charset'] = 'iso-8859-1';
        $config['newline'] = "\r\n";

        if ($mailtype_html_or_text) {
            $config['mailtype'] = $mailtype_html_or_text; //overwrite
        } else {
            $config['mailtype'] = $this->getMailType(); // choice
        }

        $config['validate'] = TRUE;

        return $config;
    }


}