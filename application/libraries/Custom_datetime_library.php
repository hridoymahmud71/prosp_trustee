<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 * this library depends on the custom_settings_library
 * function format
 *                  $this->CI->custom_settings_library->getASettings($a_settings_code, $a_settings_key)
 *                  returns a row
 * */

class Custom_datetime_library
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();


        //customized lib from modules/settings_module/libraries
        $this->CI->load->library('settings_module/custom_settings_library');

        $time_zone_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_zone');
        if ($time_zone_settings) {
            $time_zone = $time_zone_settings->settings_value;
        }

        if ($time_zone_settings && $time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }
    }


    public function callTimezone()  //does not work
    {
        date_default_timezone_get();
    }

    public function showTimezone()
    {
        echo date_default_timezone_get();
    }

    public function getTimezone()
    {
        return date_default_timezone_get();
    }

    public function showCurrentTimestamp()
    {
        $timezone = date_default_timezone_get();
        echo strtotime('now' . ' ' . $timezone);
    }

    public function getCurrentTimestamp()
    {
        $timezone = date_default_timezone_get();
        return strtotime('now' . ' ' . $timezone);
    }

    public function getDateFormat()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');

        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format_settings && $date_format) {
            return $date_format;
        } else {
            return 'Y-m-d';
        }
    }

    public function getTimeFormat()
    {
        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($time_format_settings && $time_format) {
            return $time_format;
        } else {
            return 'H:i:s';
        }
    }

    public function showCurrentDateAndTime()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($date_format && $time_format) {
            echo date($date_format . ' ' . $time_format);
        } else {
            echo date('Y-m-d H:i:s');
        }

    }

    public function getCurrentDateAndTime()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($date_format && $time_format) {
            return date($date_format . ' ' . $time_format);
        } else {
            return date('Y-m-d H:i:s');
        }
    }

    public function showCurrentDate()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format_settings && $date_format) {
            echo date($date_format);
        } else {
            echo date('Y-m-d');
        }
    }

    public function getCurrentDate()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');

        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format_settings && $date_format) {
            return date($date_format);
        } else {
            return date('Y-m-d');
        }
    }

    public function showCurrentTime()
    {
        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($time_format_settings && $time_format) {
            echo date($time_format);
        } else {
            echo date('H:i:s');
        }
    }

    public function getCurrentTime()
    {
        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($time_format_settings && $time_format) {
            return date($time_format);
        } else {
            return date('H:i:s');
        }
    }

    public function convert_and_return_DateToTimestamp($date_string)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format) {

            $date = DateTime::createFromFormat($date_format, $date_string);
            return $date->getTimestamp();
        } else {
            $date = DateTime::createFromFormat('Y-m-d', $date_string);
            return $date->getTimestamp();
        }
    }

    public function convert_and_show_DateToTimestamp($date_string)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format) {
            $date = DateTime::createFromFormat($date_format, $date_string);
            echo $date->getTimestamp();
        } else {
            $date = DateTime::createFromFormat('Y-m-d', $date_string);
            echo $date->getTimestamp();
        }
    }

    public function convert_and_return_DateAndTime_To_Timestamp($datetime_string)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($date_format && $time_format) {
            $date = DateTime::createFromFormat($date_format . ' ' . $time_format, $datetime_string);
            return $date->getTimestamp();
        } else {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $datetime_string);
            return $date->getTimestamp();
        }
    }

    public function convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($datetime_string, $datetime_format)
    {
        $date = DateTime::createFromFormat($datetime_format, $datetime_string);
        return $date->getTimestamp();
    }

    public function convert_and_show_DateAndTime_To_Timestamp($datetime_string)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }


        if ($date_format && $time_format) {
            $date = DateTime::createFromFormat($date_format . ' ' . $time_format, $datetime_string);
            echo $date->getTimestamp();
        } else {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $datetime_string);
            echo $date->getTimestamp();
        }
    }

    public function convert_and_show_TimestampToDateAndTime($timestamp)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($date_format && $time_format) {
            echo date($date_format . ' ' . $time_format, $timestamp);
        } else {
            echo date('Y-m-d H:i:s', $timestamp);
        }
    }

    public function convert_and_return_TimestampToDateAndTime($timestamp)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($date_format && $time_format) {
            return date($date_format . ' ' . $time_format, $timestamp);
        } else {
            return date('Y-m-d H:i:s', $timestamp);
        }
    }

    public function convert_and_show_TimestampToDate($timestamp)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format) {
            echo date($date_format, $timestamp);
        } else {
            echo date('Y-m-d', $timestamp);
        }
    }

    public function convert_and_return_TimestampToDate($timestamp)
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');
        if ($date_format_settings) {
            $date_format = $date_format_settings->settings_value;
        }

        if ($date_format) {
            return date($date_format, $timestamp);
        } else {
            return date('Y-m-d', $timestamp);
        }
    }

    public function convert_and_return_TimestampToDateTimeGivenFormat($timestamp,$date_format)
    {
        return date($date_format, $timestamp);
    }

    public function convert_and_return_TimestampToYearGivenFormat($timestamp,$date_format)
    {
        return date($date_format, $timestamp);
    }

    public function convert_and_return_TimestampToYearMonthGivenFormat($timestamp,$date_format)
    {
        return date($date_format, $timestamp);
    }

    public function convert_and_show_TimestampToTime($timestamp)
    {
        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($time_format) {
            echo date($time_format, $timestamp);
        } else {
            echo date('H:i:s', $timestamp);
        }
    }

    public function convert_and_return_TimestampToTime($timestamp)
    {
        $time_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');
        if ($time_format_settings) {
            $time_format = $time_format_settings->settings_value;
        }

        if ($time_format) {
            return date($time_format, $timestamp);
        } else {
            return date('H:i:s', $timestamp);
        }
    }


    /*---------------For jQuery UI-------------*/

    public function show_timeformat_PHP_to_jQueryUI()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');

        if ($date_format_settings && $date_format_settings->settings_value) {
            $jqueryui_format = $this->timeformat_PHP_to_jQueryUI($date_format_settings->settings_value);
            echo $jqueryui_format;
        } else {
            $jqueryui_format = $this->timeformat_PHP_to_jQueryUI('H:i:s');
            echo $jqueryui_format;
        }
    }

    public function get_timeformat_PHP_to_jQueryUI()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'time_format');

        if ($date_format_settings && $date_format_settings->settings_value) {
            //returns $jqueryui_format
            return $this->timeformat_PHP_to_jQueryUI($date_format_settings->settings_value);
        } else {
            //returns $jqueryui_format
            return $this->timeformat_PHP_to_jQueryUI('H:i:s');
        }
    }

    public function show_dateformat_PHP_to_jQueryUI()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');

        if ($date_format_settings && $date_format_settings->settings_value) {
            $jqueryui_format = $this->dateformat_PHP_to_jQueryUI($date_format_settings->settings_value);
            echo $jqueryui_format;
        } else {
            $jqueryui_format = $this->dateformat_PHP_to_jQueryUI('Y-m-d');
            echo $jqueryui_format;
        }
    }

    public function get_dateformat_PHP_to_jQueryUI()
    {
        $date_format_settings = $this->CI->custom_settings_library->getASettings('datetime_settings', 'date_format');

        if ($date_format_settings && $date_format_settings->settings_value) {
            //returns $jqueryui_format
            return $this->dateformat_PHP_to_jQueryUI($date_format_settings->settings_value);
        } else {
            //returns $jqueryui_format
            return $this->dateformat_PHP_to_jQueryUI('Y-m-d');
        }
    }


    /*
 * Matches each symbol of PHP date format standard
 * with jQuery equivalent codeword
 * @author Tristan Jahier
 */
    //do not use this
    function timeformat_PHP_to_jQueryUI($php_format)
    {
        $SYMBOLS_MATCHING = array(
            // Day
            'd' => '',
            'D' => '',
            'j' => '',
            'l' => '',
            'N' => '',
            'S' => '',
            'w' => '',
            'z' => '',
            // Week
            'W' => '',
            // Month
            'F' => '',
            'm' => '',
            'M' => '',
            'n' => '',
            't' => '',
            // Year
            'L' => '',
            'o' => '',
            'Y' => '',
            'y' => '',
            // Time
            'a' => 'tt',
            'A' => 'TT',
            'B' => '',
            'g' => 'h',
            'G' => 'H',
            'h' => 'hh',
            'H' => 'HH',
            'i' => 'mm',
            's' => 'ss',
            'u' => ''
        );
        $jqueryui_format = "";
        $escaping = false;
        for ($i = 0; $i < strlen($php_format); $i++) {
            $char = $php_format[$i];
            if ($char === '\\') // PHP date format escaping character
            {
                $i++;
                if ($escaping) $jqueryui_format .= $php_format[$i];
                else $jqueryui_format .= '\'' . $php_format[$i];
                $escaping = true;
            } else {
                if ($escaping) {
                    $jqueryui_format .= "'";
                    $escaping = false;
                }
                if (isset($SYMBOLS_MATCHING[$char]))
                    $jqueryui_format .= $SYMBOLS_MATCHING[$char];
                else
                    $jqueryui_format .= $char;
            }
        }
        return $jqueryui_format;
    }

    function dateformat_PHP_to_jQueryUI($php_format)
    {
        $SYMBOLS_MATCHING = array(
            // Day
            'd' => 'dd',
            'D' => 'D',
            'j' => 'd',
            'l' => 'DD',
            'N' => '',
            'S' => '',
            'w' => '',
            'z' => 'o',
            // Week
            'W' => '',
            // Month
            'F' => 'MM',
            'm' => 'mm',
            'M' => 'M',
            'n' => 'm',
            't' => '',
            // Year
            'L' => '',
            'o' => '',
            'Y' => 'yy',
            'y' => 'y',
            // Time
            'a' => '',
            'A' => '',
            'B' => '',
            'g' => '',
            'G' => '',
            'h' => '',
            'H' => '',
            'i' => '',
            's' => '',
            'u' => ''
        );
        $jqueryui_format = "";
        $escaping = false;
        for ($i = 0; $i < strlen($php_format); $i++) {
            $char = $php_format[$i];
            if ($char === '\\') // PHP date format escaping character
            {
                $i++;
                if ($escaping) $jqueryui_format .= $php_format[$i];
                else $jqueryui_format .= '\'' . $php_format[$i];
                $escaping = true;
            } else {
                if ($escaping) {
                    $jqueryui_format .= "'";
                    $escaping = false;
                }
                if (isset($SYMBOLS_MATCHING[$char]))
                    $jqueryui_format .= $SYMBOLS_MATCHING[$char];
                else
                    $jqueryui_format .= $char;
            }
        }
        return $jqueryui_format;
    }

    /*--------------------------------------------------------------------------*/

    public function checkIsToday_byTimestamp($timestamp)
    {

    }

    public function checkIsToday_byTimestampAndGivenDateFormat($given_timestamp, $given_date_format)
    {
        $current_timestamp = $this->getCurrentTimestamp();

        $current_date_string = date($given_date_format, $current_timestamp);
        $given_date_string = date($given_date_format, $given_timestamp);

        if ($current_date_string == $given_date_string) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIsYesterday_byTimestampAndGivenDateFormat($given_timestamp, $given_date_format)
    {
        $a_day_before_current_date_string = date($given_date_format, strtotime("yesterday"));

        $given_date_string = date($given_date_format, $given_timestamp);

        if ($a_day_before_current_date_string == $given_date_string) {
            return true;
        } else {
            return false;
        }
    }


}






